package net.ming616.nlp.owl.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.base.service.BaseManagerTestCase;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-ontology.xml" })
public class MyOWLOntologyManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MyOWLOntologyManagerTest.class);

	String baseURI = "http://nlp.bit.edu.cn/ontologies/computer";

	@Autowired
	OntologyMaintainManager myOntologyManager;

	String computerFile = "e:/ming616/data/owl/computer_class.txt";

	String productFile = "e:/ming616/data/owl/computer_product.txt";

	/**
	 * Generating product according URL;
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private void createProduct(OWLClass productClass, String productURL)
			throws MalformedURLException, IOException {

		Document doc = Jsoup.parse(new URL(productURL), 6000);

		String productId = doc.select("#name").first().attr("pshowskuid");
		String productName = doc.select("a[href=" + productURL + "]").first()
				.text();

		OWLNamedIndividual individual = this.myOntologyManager
				.createIndividual(productName + "_" + productId);
		this.myOntologyManager
				.createIndividualOfClass(productClass, individual);

		Elements trs = doc.select(".Ptable tr");

		for (Element tr : trs) {
			Elements tds = tr.select("td");
			if (tds != null && tds.size() == 2) {
				String propertyName = tds.get(0).text();
				String value = tds.get(1).text();
				this.myOntologyManager.createRelationForIndividualAndData(
						individual, propertyName, null, value);

			}
		}
		this.myOntologyManager.saveOntology();
	}

	public void createProductClasses() throws IOException {
		OutputStream out = null;
		try {
			File file = new File(computerFile);
			if (file.exists()) {
				FileUtils.deleteQuietly(file);
			}
			out = new FileOutputStream(computerFile, true);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		Map<String, OWLClass> classMap = new HashMap<String, OWLClass>();
		String url = "http://www.360buy.com/computer.html";
		Document doc = null;
		try {
			doc = Jsoup.parse(new URL(url), 6000);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Element left = doc.select(".left #sortlist").first();
		String root = left.select(".mt").text();
		OWLClass rootClass = this.myOntologyManager.createOWLClass(root);
		Elements current = left.select(".mc div");
		for (Element element : current) {
			String second = element.select("h3").text();
			OWLClass secondClass = this.myOntologyManager
					.createOWLClass(second);
			this.myOntologyManager.createSubClassRelation(secondClass,
					rootClass);
			Elements lis = element.select("li a");
			for (Element e : lis) {
				OWLClass third = this.myOntologyManager
						.createOWLClass(e.text());
				String productClassHref = e.attr("href");
				this.myOntologyManager.createSubClassRelation(third,
						secondClass);
				classMap.put(productClassHref, third);
				IOUtils.write(productClassHref + "=" + third.getIRI() + "\n",
						out);
			}
		}
		this.myOntologyManager.saveOntology();
		logger.info("computer hardware class generated !");
	}

	@Test
	public void createProducts() throws IOException {
		// this.createProductClasses();
		this.generateProductURLs();
		List<String> lines = FileUtils.readLines(new File(this.productFile));
		for (String line : lines) {
			String url = StringUtils.substringBefore(line, "=");
			String className = StringUtils.substringAfterLast(line, "=");
			OWLClass cls = this.myOntologyManager.createOWLClass(className);
			try {
				this.createProduct(cls, url);
			} catch (Exception e) {
				logger.error(e.getMessage());
				continue;
			}

		}

	}

	public void generateProductURLs() throws IOException {
		List<String> lines = FileUtils.readLines(new File(this.computerFile));
		Map<String, OWLClass> pages = new HashMap<String, OWLClass>();
		Map<String, OWLClass> productURLs = new HashMap<String, OWLClass>();
		for (String line : lines) {
			String url = StringUtils.substringBefore(line, "=");
			String urlPath = StringUtils.substringBeforeLast(url, "/");
			String conceptName = StringUtils.substringAfter(line, "=");
			OWLClass cls =this.myOntologyManager.createOWLClass(conceptName);
			Document doc = null;
			try {
				doc = Jsoup.parse(new URL(url), 6000);
			} catch (Exception e) {
				logger.error(e.getMessage());
				continue;
			}
			Elements hrefs = doc.select(".pagin a");
			if (null != hrefs && hrefs.size() > 2) {
				String href = hrefs.get(hrefs.size() - 2).attr("href");
				// String newhref = StringUtils.substringAfterLast(href, "/");
				String before = StringUtils.substringBeforeLast(href, "-");
				String after = StringUtils.substringAfterLast(href, ".");
				String num = StringUtils.substringBetween(href, before + "-",
						"." + after);
				int number = Integer.parseInt(num);
				for (int i = 1; i <= number; i++) {
					String pageURL = urlPath + "/" + before + "-" + i + "."
							+ after;
					if (logger.isInfoEnabled()) {
						logger.info("pageURL=" + pageURL); //$NON-NLS-1$
					}
					pages.put(pageURL, cls);
				}
			}
		}

		FileOutputStream out = null;
		try {
			File file = new File(this.productFile);
			if (file.exists()) {
				FileUtils.deleteQuietly(file);
			}
			out = new FileOutputStream(this.productFile, true);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		for (Entry<String, OWLClass> entry : pages.entrySet()) {
			Document doc = null;
			URL url = new URL(entry.getKey());
			try {
				doc = Jsoup.parse(url, 6000);
			} catch (Exception e) {
				logger.error(e.getMessage());
				continue;
			}
			Elements hrefs = doc.select("#plist .p-name a");
			for (Element element : hrefs) {
				String producthref = element.attr("href");
				OWLClass cls = entry.getValue();
				productURLs.put(producthref, entry.getValue());
				IOUtils.write(producthref + "=" + cls.getIRI() + "\n", out);
			}
		}

	}
}
