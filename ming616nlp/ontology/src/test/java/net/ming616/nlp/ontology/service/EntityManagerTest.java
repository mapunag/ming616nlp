package net.ming616.nlp.ontology.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.ontology.model.OntologyEntity;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-ontology.xml" })
public class EntityManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntityManagerTest.class);

	@Autowired
	EntityManager entityManager;

	@Test
	public void testSave() {
		OntologyEntity entity = new OntologyEntity();
		entity.setName("Display");
		entity.setText("显示器");
		entity.setText_en_us("Display,Mornitor");
		entity.setText_zh_cn("显示器,监视器");
		entity = this.entityManager.save(entity);
		if (logger.isInfoEnabled()) {
			logger.info("testSave() - OntologyEntity entity=" + entity); //$NON-NLS-1$
		}
	}

	@Test
	@Ignore
	public void testFindByNamedQuery() {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("name", "haha");
		List<OntologyEntity> list = this.entityManager.findByNamedQuery(
				"entityQueryByName", queryParams);
		if (logger.isInfoEnabled()) {
			logger.info(" list=" + list); //$NON-NLS-1$
		}

	}
}
