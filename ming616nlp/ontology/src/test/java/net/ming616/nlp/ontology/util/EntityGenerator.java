package net.ming616.nlp.ontology.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.ming616.nlp.ontology.model.OntologyEntity;
import net.ming616.nlp.ontology.model.OntologyEntityAttribute;
import net.ming616.nlp.ontology.model.OntologyEntityAttributeValue;
import net.ming616.nlp.ontology.service.AttributeManager;
import net.ming616.nlp.ontology.service.AttributeValueManager;
import net.ming616.nlp.ontology.service.EntityManager;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EntityGenerator {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntityGenerator.class);

	EntityManager entityManager;
	AttributeManager attributeManager;
	AttributeValueManager attributeValueManager;

	static String[] xmls = new String[] { "applicationContext-base.xml",
			"applicationContextTest-resource.xml",
			"applicationContext-dao.xml", "applicationContext-ontology.xml" };

	ClassPathXmlApplicationContext context;

	public void init() {
		if (context == null) {
			context = new ClassPathXmlApplicationContext(xmls);
		}

		entityManager = (EntityManager) context.getBean("entityManager");
		attributeManager = (AttributeManager) context
				.getBean("attributeManager");
		attributeValueManager = (AttributeValueManager) context
				.getBean("attributeValueManager");
	}

	public void generate(HashMap<String, String> attValueMap) {
		OntologyEntity e = new OntologyEntity();
		e.setName("display");
		e.setText("display");
		e.setText_zh_cn("显示器");
		e.setText_en_us("display");
		Iterator it = attValueMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it
					.next();
			OntologyEntityAttribute a = new OntologyEntityAttribute();
			a.setName(entry.getKey());
			a.setText(entry.getKey());
			a.setText_zh_cn(entry.getKey());
			a.setText_en_us("null");

			OntologyEntityAttributeValue v = new OntologyEntityAttributeValue();
			v.setName(entry.getKey());
			v.setText_zh_cn(entry.getValue());
			v.setText_en_us("null");
			v = this.attributeValueManager.save(v);

			a.getValues().add(v);
			a = this.attributeManager.save(a);
			e.getAttributes().add(a);
		}
		this.entityManager.save(e);
	}

	public ArrayList<String> extractUrl() {
		ArrayList<String> productUrlList = new ArrayList<String>();
		ArrayList<String> pageUrlList = new ArrayList<String>();
		for (int i = 1; i < 11; i++) {
			String pageUrl = "http://www.360buy.com/products/670-677-688-0-0-0-0-0-0-0-1-1-"
					+ i + ".html";
			pageUrlList.add(pageUrl);
		}
		Document doc;
		for (int i = 0; i < pageUrlList.size(); i++) {
			String pageUrl = pageUrlList.get(i);
			try {
				doc = Jsoup.parse(new URL(pageUrl), 60000);
				Elements as = doc.select(".p-name a");
				for (Element e : as) {
					String s = e.attr("href");
					productUrlList.add(s);
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return productUrlList;
	}

	public HashMap<String, String> extract(String url) {
		HashMap<String, String> attValueMap = new HashMap<String, String>();
		Document doc;
		try {
			doc = Jsoup.parse(new URL(url), 60000);
			Elements trs = doc.select(".Ptable tr");
			for (Element e : trs) {
				Elements tds = e.select("td");
				if (null != tds && tds.size() == 2) {
					String att = tds.get(0).text();
					String value = tds.get(1).text();
					attValueMap.put(att, value);
					logger.info(att + "=" + value);
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attValueMap;
	}

	public static void main(String[] args) {
		EntityGenerator gen = new EntityGenerator();
		gen.init();
		// gen.generate();
		// String url = "http://www.360buy.com/product/358114.html";
		// gen.extract(url);
		ArrayList<String> pul = gen.extractUrl();
		for (String url : pul) {
			gen.generate(gen.extract(url));
		}
		System.exit(0);
	}
}
