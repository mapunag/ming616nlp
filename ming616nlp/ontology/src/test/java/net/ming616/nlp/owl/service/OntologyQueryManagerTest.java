package net.ming616.nlp.owl.service;

import java.util.ArrayList;
import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-ontology.xml" })
public class OntologyQueryManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(OntologyQueryManagerTest.class);

	@Autowired
	OntologyQueryManager ontologyQueryManager;

	@Test
	public void queryIndividualByValues() {

		List<String> values = new ArrayList<String>();
		values.add("1920 x 1080");
		values.add("飞利浦");
		values.add("23.6英寸");
		List<OWLNamedIndividual> result = this.ontologyQueryManager
				.getIndividualsByValues(values);
		if (logger.isInfoEnabled()) {
			logger.info("result=" + result); //$NON-NLS-1$
		}
	}
	
	
	
	// @Test
	// public void queryProduct() {
	// OWLNamedIndividual individual = this.ontologyMaintainManager
	// .createIndividual("飞利浦202E1SB_185239");
	// if (logger.isInfoEnabled()) {
	//			logger.info("queryProduct() - OWLNamedIndividual individual=" + individual); //$NON-NLS-1$
	// }
	//
	// Map<OWLDataPropertyExpression, Set<OWLLiteral>> values = individual
	// .getDataPropertyValues(null);
	//
	// for (Entry<OWLDataPropertyExpression, Set<OWLLiteral>> v : values
	// .entrySet()) {
	// for (OWLLiteral property : v.getValue()) {
	// logger.info(v.getKey().asOWLDataProperty() + "="
	// + property.getLiteral());
	// }
	// }
	// }

	// @Test
	// public void queryProductByValue() {
	// OWLLiteral lit = this.dataFactory.getOWLLiteral("黑色");
	// OWLDataProperty dataProperty = dataFactory.getOWLDataProperty(IRI
	// .create("#" + "颜色"));
	//
	// if (logger.isInfoEnabled()) {
	//			logger.info("queryProductByValue() - OWLLiteral lit=" + lit.getLiteral()); //$NON-NLS-1$
	// }
	//
	// }
}
