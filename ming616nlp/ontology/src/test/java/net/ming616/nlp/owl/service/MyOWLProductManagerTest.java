package net.ming616.nlp.owl.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-ontology.xml" })
public class MyOWLProductManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MyOWLProductManagerTest.class);

	@Autowired
	OntologyMaintainManager myOntologyManager;

	// String computerClassFile = "e:/ming616/data/owl/computer_class.owl";

	String computerFile = "e:/ming616/data/owl/computer_class.txt";

	String productFile = "e:/ming616/data/owl/computer_product.txt";

	/**
	 * Generating product according URL;
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private void createProduct(OWLClass productClass, String productURL)
			throws MalformedURLException, IOException {

		Document doc = Jsoup.parse(new URL(productURL), 6000);

		String productId = doc.select("#name").first().attr("pshowskuid");
		String productName = doc.select("a[href=" + productURL + "]").first()
				.text();

		OWLNamedIndividual individual = this.myOntologyManager
				.createIndividual(productName + "_" + productId);
		this.myOntologyManager
				.createIndividualOfClass(productClass, individual);

		Elements trs = doc.select(".Ptable tr");
		for (Element tr : trs) {
			Elements tds = tr.select("td");
			if (tds != null && tds.size() == 2) {
				String propertyName = tds.get(0).text();
				String valueName = tds.get(1).text();
				this.myOntologyManager.createRelationForIndividualAndData(
						individual, propertyName, null, valueName);

			}
		}
		this.myOntologyManager.saveOntology();
	}

	@Test
	public void createProducts() throws IOException {
		// this.createProductClasses();
		// this.generateProductURLs();
		List<String> lines = FileUtils.readLines(new File(this.productFile));
		for (String line : lines) {
			String url = StringUtils.substringBefore(line, "=");
			String conceptName = StringUtils.substringAfterLast(line, "=");
			OWLClass cls = this.myOntologyManager.createOWLClass(conceptName);
			try {
				this.createProduct(cls, url);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}
}
