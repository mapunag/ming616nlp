package net.ming616.nlp.ontology.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.extraction.model.JingdongProduct;
import net.ming616.nlp.extraction.service.JingdongExtractor;
import net.ming616.nlp.ontology.model.DataUnit;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = {
		"classpath:applicationContext-ontology.xml",
		"classpath:applicationContext-extraction.xml" })
public class YagoManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(YagoManagerTest.class);

	@Autowired
	JingdongExtractor jingdongExtractor;

	@Autowired
	RDFManager rdfManager;

	String resourceName = "display";

	// @Test
	public void testGenerateEntry() {
		String url = "http://www.360buy.com/products/670-677-000.html";
		Map<String, String> itemMap = this.jingdongExtractor
				.getProductURLList(url);
		for (Entry<String, String> item : itemMap.entrySet()) {
			JingdongProduct product = this.jingdongExtractor.getProduct(item
					.getValue());
			Map<String, List<String>> attributeMap = product.getAttributes();
			for (Entry<String, List<String>> attributeEntry : attributeMap
					.entrySet()) {
				String name = attributeEntry.getKey();
				List<String> valueList = attributeEntry.getValue();
				for (String value : valueList) {
					this.rdfManager.addProperty(product.getName(), name, value);
				}
			}
		}
	}

	@Test
	public void testQuery() {
		List<DataUnit> subject = this.rdfManager.findBySubject("Dell");
		if (logger.isInfoEnabled()) {
			logger.info("subject=" + subject); //$NON-NLS-1$
		}
	}
}
