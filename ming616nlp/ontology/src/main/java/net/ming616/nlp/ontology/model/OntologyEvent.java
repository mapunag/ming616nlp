package net.ming616.nlp.ontology.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "ontologyEvent")
@Table(name = "T_ONTOLOGY_EVENT")
@NamedQueries({
		@NamedQuery(name = "eventQueryByName", query = "SELECT OBJECT(onto) FROM ontologyEvent onto WHERE onto.name = :name"),
		@NamedQuery(name = "eventQueryByText", query = "SELECT OBJECT(onto) FROM ontologyEvent onto WHERE onto.text = :text") })
public class OntologyEvent extends OntologyModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6646014717103529254L;

	@OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
	List<OntologyEventArgument> arguments = new ArrayList<OntologyEventArgument>();;

	@OneToMany(mappedBy = "firstEvent", fetch = FetchType.EAGER)
	Set<OntologyEventRelation> firstRelations = new HashSet<OntologyEventRelation>();

	@OneToMany(mappedBy = "secondEvent", fetch = FetchType.EAGER)
	Set<OntologyEventRelation> secondRelations = new HashSet<OntologyEventRelation>();

}
