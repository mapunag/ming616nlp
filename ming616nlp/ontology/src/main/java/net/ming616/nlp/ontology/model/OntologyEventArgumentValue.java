package net.ming616.nlp.ontology.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name = "argumentValue")
@Table(name = "T_ONTOLOGY_EVENT_ARGUMENT_VALUE")
@NamedQueries({
		@NamedQuery(name = "argumentValueQueryByName", query = "SELECT OBJECT(onto) FROM argumentValue onto WHERE onto.name = :name"),
		@NamedQuery(name = "argumentValueQueryByText", query = "SELECT OBJECT(onto) FROM argumentValue onto WHERE onto.text = :text") })
public class OntologyEventArgumentValue extends OntologyModel {

	private static final long serialVersionUID = -6363411910034222223L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ARGUMENT_VALUE_ID")
	OntologyEventArgument argument;

	public OntologyEventArgument getArgument() {
		return argument;
	}

	public void setArgument(OntologyEventArgument argument) {
		this.argument = argument;
	}

}
