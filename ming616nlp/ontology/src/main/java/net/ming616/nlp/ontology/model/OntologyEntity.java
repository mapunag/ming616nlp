package net.ming616.nlp.ontology.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "ontologyEntity")
@Table(name = "T_ONTOLOGY_ENTITY")
@NamedQueries( {
		@NamedQuery(name = "entityQueryByName", query = "SELECT OBJECT(onto) FROM ontologyEntity onto WHERE onto.name = :name"),
		@NamedQuery(name = "entityQueryByText", query = "SELECT OBJECT(onto) FROM ontologyEntity onto WHERE onto.text = :text") })
public class OntologyEntity extends OntologyModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6646014717103529254L;

	@OneToMany(mappedBy = "entity", fetch = FetchType.LAZY)
	List<OntologyEntityAttribute> attributes = new ArrayList<OntologyEntityAttribute>();;

	@OneToMany(mappedBy = "firstEntity", fetch = FetchType.LAZY)
	Set<OntologyEntityRelation> firstRelations = new HashSet<OntologyEntityRelation>();

	@OneToMany(mappedBy = "secondEntity", fetch = FetchType.LAZY)
	Set<OntologyEntityRelation> secondRelations = new HashSet<OntologyEntityRelation>();

	public OntologyEntity() {
		this.type = "ontologyEntity";
	}

	public List<OntologyEntityAttribute> getAttributes() {
		
		return attributes;
	}

	public Set<OntologyEntityRelation> getFirstRelations() {
		return firstRelations;
	}

	public Set<OntologyEntityRelation> getSecondRelations() {
		return secondRelations;
	}

	public void setAttributes(List<OntologyEntityAttribute> attributes) {
		this.attributes = attributes;
	}

	public void setFirstRelations(Set<OntologyEntityRelation> firstRelations) {
		this.firstRelations = firstRelations;
	}

	public void setSecondRelations(Set<OntologyEntityRelation> secondRelations) {
		this.secondRelations = secondRelations;
	}

}
