package net.ming616.nlp.owl.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntologyChange;

public interface OntologyMaintainManager {

	OWLNamedIndividual createIndividual(String individualName);

	void createIndividualOfClass(OWLClass owlcls, OWLIndividual owlIndividual);

	public List<OWLOntologyChange> createRelationForIndividualAndData(
			OWLIndividual individual, String dataPropertyName,
			String dataPropertyType, String dataPropertyValue);

	OWLClass createOWLClass(String conceptName);

	OWLObjectProperty createObjectProperty(String propertyName);

	OWLDataProperty createDataProperty(String propertyName);

	void createRelationOfIndividuals(OWLObjectProperty property,
			OWLIndividual owlIndividual1, OWLIndividual owlIndivididual2);

	void createSubClassRelation(OWLClass clsChild, OWLClass clsParent);

	Set<OWLIndividual> getIndividualsOfClass(OWLClass owlcls);

	Map<OWLObjectPropertyExpression, Set<OWLIndividual>> getObjectPropertyValues(
			OWLNamedIndividual owlIndi);

	Set<OWLClassExpression> getSubClasses(OWLClass owlcls);

	List<OWLOntologyChange> addAxiom(OWLAxiom axiom);

	void saveOntology();

}
