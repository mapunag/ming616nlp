package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.ArgumentDao;
import net.ming616.nlp.ontology.model.OntologyEventArgument;

import org.springframework.stereotype.Service;

@Service("argumentDao")
public class ArgumentDaoImpl extends
		GenericDaoImpl<OntologyEventArgument, Long> implements ArgumentDao {

	public ArgumentDaoImpl() {
		super(OntologyEventArgument.class);
	}

}
