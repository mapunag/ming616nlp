package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.EntityRelationDao;
import net.ming616.nlp.ontology.model.OntologyEntityRelation;
import net.ming616.nlp.ontology.service.EntityRelationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("entityRelationManager")
@Transactional
public class EntityRelationMangerImpl extends
		GenericManagerImpl<OntologyEntityRelation, Long> implements
		EntityRelationManager {
	EntityRelationDao entityRelationDao;

	@Autowired
	public void setEntityRelationDao(EntityRelationDao entityRelationDao) {
		this.entityRelationDao = entityRelationDao;
		this.dao = this.entityRelationDao;
	}

}
