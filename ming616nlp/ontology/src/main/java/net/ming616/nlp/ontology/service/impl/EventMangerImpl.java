package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.EventDao;
import net.ming616.nlp.ontology.model.OntologyEvent;
import net.ming616.nlp.ontology.service.EventManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("eventManager")
@Transactional
public class EventMangerImpl extends GenericManagerImpl<OntologyEvent, Long>
		implements EventManager {
	EventDao eventDao;

	@Autowired
	public void setEntityDao(EventDao eventDao) {
		this.eventDao = eventDao;
		this.dao = this.eventDao;
	}

}
