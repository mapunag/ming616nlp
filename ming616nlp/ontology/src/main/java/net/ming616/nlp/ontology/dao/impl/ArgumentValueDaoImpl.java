package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.ArgumentValueDao;
import net.ming616.nlp.ontology.model.OntologyEventArgumentValue;

import org.springframework.stereotype.Service;

@Service("argumentValueDao")
public class ArgumentValueDaoImpl extends
		GenericDaoImpl<OntologyEventArgumentValue, Long> implements
		ArgumentValueDao {

	public ArgumentValueDaoImpl() {
		super(OntologyEventArgumentValue.class);
	}

}
