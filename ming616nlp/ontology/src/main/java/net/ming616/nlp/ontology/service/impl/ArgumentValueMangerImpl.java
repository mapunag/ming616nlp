package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.ArgumentValueDao;
import net.ming616.nlp.ontology.model.OntologyEventArgumentValue;
import net.ming616.nlp.ontology.service.ArgumentValueManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("argumentValueManager")
@Transactional
public class ArgumentValueMangerImpl extends
		GenericManagerImpl<OntologyEventArgumentValue, Long> implements
		ArgumentValueManager {
	ArgumentValueDao argumentValueDao;

	@Autowired
	public void setArgumentValueDao(ArgumentValueDao argumentValueDao) {
		this.argumentValueDao = argumentValueDao;
		this.dao = this.argumentValueDao;
	}

}
