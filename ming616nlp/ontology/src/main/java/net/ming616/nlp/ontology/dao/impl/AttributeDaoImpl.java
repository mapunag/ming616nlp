package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.AttributeDao;
import net.ming616.nlp.ontology.model.OntologyEntityAttribute;

import org.springframework.stereotype.Service;

@Service("attributeDao")
public class AttributeDaoImpl extends
		GenericDaoImpl<OntologyEntityAttribute, Long> implements AttributeDao {

	public AttributeDaoImpl() {
		super(OntologyEntityAttribute.class);
	}

}
