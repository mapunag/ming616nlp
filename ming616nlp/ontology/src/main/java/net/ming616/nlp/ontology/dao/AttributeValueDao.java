package net.ming616.nlp.ontology.dao;

import net.ming616.nlp.base.dao.GenericDao;
import net.ming616.nlp.ontology.model.OntologyEntityAttributeValue;

public interface AttributeValueDao extends
		GenericDao<OntologyEntityAttributeValue, Long> {

}
