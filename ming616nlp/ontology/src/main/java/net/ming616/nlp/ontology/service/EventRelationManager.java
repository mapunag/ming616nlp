package net.ming616.nlp.ontology.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.ontology.model.OntologyEventRelation;

public interface EventRelationManager extends
		GenericManager<OntologyEventRelation, Long> {
}
