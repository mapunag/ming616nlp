package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.AttributeValueDao;
import net.ming616.nlp.ontology.model.OntologyEntityAttributeValue;

import org.springframework.stereotype.Service;

@Service("attributeValueDao")
public class AttributeValueDaoImpl extends
		GenericDaoImpl<OntologyEntityAttributeValue, Long> implements
		AttributeValueDao {

	public AttributeValueDaoImpl() {
		super(OntologyEntityAttributeValue.class);
	}

}
