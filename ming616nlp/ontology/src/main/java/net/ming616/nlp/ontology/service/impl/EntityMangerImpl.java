package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.EntityDao;
import net.ming616.nlp.ontology.model.OntologyEntity;
import net.ming616.nlp.ontology.service.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("entityManager")
@Transactional
public class EntityMangerImpl extends GenericManagerImpl<OntologyEntity, Long>
		implements EntityManager {
	EntityDao entityDao;

	@Autowired
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
		this.dao = this.entityDao;
	}

}
