package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.AttributeValueDao;
import net.ming616.nlp.ontology.model.OntologyEntityAttributeValue;
import net.ming616.nlp.ontology.service.AttributeValueManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("attributeValueManager")
@Transactional
public class AttributeValueMangerImpl extends
		GenericManagerImpl<OntologyEntityAttributeValue, Long> implements
		AttributeValueManager {
	AttributeValueDao attributeValueDao;

	@Autowired
	public void setAttributeValueDao(AttributeValueDao attributeValueDao) {
		this.attributeValueDao = attributeValueDao;
		this.dao = this.attributeValueDao;
	}

}
