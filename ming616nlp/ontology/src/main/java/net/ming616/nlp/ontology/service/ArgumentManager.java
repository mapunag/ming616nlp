package net.ming616.nlp.ontology.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.ontology.model.OntologyEventArgument;

public interface ArgumentManager extends
		GenericManager<OntologyEventArgument, Long> {
}
