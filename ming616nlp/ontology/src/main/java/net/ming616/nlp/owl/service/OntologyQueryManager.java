package net.ming616.nlp.owl.service;

import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLNamedIndividual;

public interface OntologyQueryManager {

	List<OWLNamedIndividual> getIndividualsByAttributeAndValue(
			Map<String, String> attrs);

	List<OWLNamedIndividual> getIndividualsByValues(List<String> values);

	String getValueByProperty(String property, OWLNamedIndividual individual);

}
