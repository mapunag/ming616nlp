package net.ming616.nlp.ontology.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.ontology.model.OntologyEntity;

public interface EntityManager extends GenericManager<OntologyEntity, Long> {
}
