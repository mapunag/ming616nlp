package net.ming616.nlp.ontology.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "ontologyEntityAttribute")
@Table(name = "T_ONTOLOGY_ENTITY_ATTRIBUTE")
@NamedQueries({
		@NamedQuery(name = "attributeQueryByName", query = "SELECT OBJECT(onto) FROM ontologyEntityAttribute onto WHERE onto.name = :name"),
		@NamedQuery(name = "attributeQueryByText", query = "SELECT OBJECT(onto) FROM ontologyEntityAttribute onto WHERE onto.text = :text") })
public class OntologyEntityAttribute extends OntologyModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5639701598575887985L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ENTITY_ID")
	OntologyEntity entity;

	@OneToMany(mappedBy = "attribute", fetch = FetchType.LAZY)
	Set<OntologyEntityAttributeValue> values = new HashSet<OntologyEntityAttributeValue>();

	public Set<OntologyEntityAttributeValue> getValues() {
		return values;
	}

	public void setValues(Set<OntologyEntityAttributeValue> values) {
		this.values = values;
	}

	public OntologyEntity getEntity() {
		return entity;
	}

	public void setEntity(OntologyEntity entity) {
		this.entity = entity;
	}

	public OntologyEntityAttribute() {
		this.type = "ontologyEntityAttribute";
	}

}
