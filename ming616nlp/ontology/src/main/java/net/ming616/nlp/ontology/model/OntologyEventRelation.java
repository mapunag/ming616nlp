package net.ming616.nlp.ontology.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ontologyEventRelation")
@Table(name = "T_ONTOLOGY_EVENT_RELATION")
public class OntologyEventRelation extends OntologyModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3480546405230992277L;
	@Column(name = "RELATION_NAME")
	String relationName;

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

	public OntologyEvent getFirstEvent() {
		return firstEvent;
	}

	public void setFirstEvent(OntologyEvent firstEvent) {
		this.firstEvent = firstEvent;
	}

	public OntologyEvent getSecondEvent() {
		return secondEvent;
	}

	public void setSecondEvent(OntologyEvent secondEvent) {
		this.secondEvent = secondEvent;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FIRST_ID")
	OntologyEvent firstEvent;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SECOND_ID")
	OntologyEvent secondEvent;

}
