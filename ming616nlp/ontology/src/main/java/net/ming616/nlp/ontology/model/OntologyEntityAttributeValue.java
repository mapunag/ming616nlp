package net.ming616.nlp.ontology.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name = "ontologyEntityAttributeValue")
@Table(name = "T_ONTOLOGY_ENTITY_ATTRIBUTE_VALUE")
@NamedQueries({
		@NamedQuery(name = "attributeValueQueryByName", query = "SELECT OBJECT(onto) FROM ontologyEntityAttributeValue onto WHERE onto.name = :name"),
		@NamedQuery(name = "attributeValueQueryByText", query = "SELECT OBJECT(onto) FROM ontologyEntityAttributeValue onto WHERE onto.text = :text") })
public class OntologyEntityAttributeValue extends OntologyModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5639701598575887985L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ATTRIBUTE_ID")
	OntologyEntityAttribute attribute;

	public OntologyEntityAttributeValue() {
		this.type = "ontologyEntityAttributeValue";
	}

	public OntologyEntityAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(OntologyEntityAttribute attribute) {
		this.attribute = attribute;
	}

}
