package net.ming616.nlp.ontology.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.ontology.model.OntologyEntityAttribute;

public interface AttributeManager extends
		GenericManager<OntologyEntityAttribute, Long> {
}
