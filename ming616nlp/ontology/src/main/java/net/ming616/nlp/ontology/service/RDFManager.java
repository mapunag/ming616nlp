package net.ming616.nlp.ontology.service;

import java.util.List;

import net.ming616.nlp.ontology.model.DataUnit;

import com.hp.hpl.jena.rdf.model.Resource;


public interface RDFManager {

	Resource generateResource(String resourceURI);

	Resource addProperty(String resourceName, String propertyName,
			String propertyValue);

	List<DataUnit> findByObject(String obj);

	List<DataUnit> findBySubject(String subject);

}
