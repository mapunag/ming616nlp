package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.EventRelationDao;
import net.ming616.nlp.ontology.model.OntologyEventRelation;

import org.springframework.stereotype.Service;

@Service("eventRelationDao")
public class EventRelationDaoImpl extends
		GenericDaoImpl<OntologyEventRelation, Long> implements EventRelationDao {

	public EventRelationDaoImpl() {
		super(OntologyEventRelation.class);
	}

}
