package net.ming616.nlp.owl.utils;

public class XMLUtils {
	public static String stripNonValidXMLCharacters(String xmlStr) {
		StringBuilder sbud = new StringBuilder();
		for (char c : xmlStr.toCharArray()) {
			if (!(0x00 < c && c < 0x08 || 0x0b < c && c < 0x0c || 0x0e < c
					&& c < 0x1f)) {
				sbud.append(c);
			}
		}
		return sbud.toString();
	}
}
