package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.EntityRelationDao;
import net.ming616.nlp.ontology.model.OntologyEntityRelation;

import org.springframework.stereotype.Service;

@Service("entityRelationDao")
public class EntityRelationDaoImpl extends
		GenericDaoImpl<OntologyEntityRelation, Long> implements
		EntityRelationDao {

	public EntityRelationDaoImpl() {
		super(OntologyEntityRelation.class);
	}

}
