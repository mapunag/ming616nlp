package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.AttributeDao;
import net.ming616.nlp.ontology.model.OntologyEntityAttribute;
import net.ming616.nlp.ontology.service.AttributeManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("attributeManager")
@Transactional
public class AttributeMangerImpl extends
		GenericManagerImpl<OntologyEntityAttribute, Long> implements
		AttributeManager {
	AttributeDao attributeDao;

	@Autowired
	public void setAttributeDao(AttributeDao attributeDao) {
		this.attributeDao = attributeDao;
		this.dao = this.attributeDao;
	}

}
