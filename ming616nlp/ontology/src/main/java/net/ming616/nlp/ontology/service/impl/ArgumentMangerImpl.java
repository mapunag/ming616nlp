package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.ArgumentDao;
import net.ming616.nlp.ontology.model.OntologyEventArgument;
import net.ming616.nlp.ontology.service.ArgumentManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("argumentManager")
@Transactional
public class ArgumentMangerImpl extends
		GenericManagerImpl<OntologyEventArgument, Long> implements
		ArgumentManager {
	ArgumentDao argumentDao;

	@Autowired
	public void setArgumentDao(ArgumentDao argumentDao) {
		this.argumentDao = argumentDao;
		this.dao = this.argumentDao;
	}

}
