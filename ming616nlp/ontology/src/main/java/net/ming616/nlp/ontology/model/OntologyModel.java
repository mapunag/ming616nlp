package net.ming616.nlp.ontology.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import net.ming616.nlp.base.model.BaseModel;

@MappedSuperclass
public class OntologyModel extends BaseModel {
	private static final long serialVersionUID = 1547096516153883788L;

	@Column(name = "ONTOLOGY_NAME")
	String name;

	@Column(name = "ONTOLOGY_TEXT")
	String text;

	@Column(name = "ONTOLOGY_TEXT_EN_US")
	String text_en_us;

	@Column(name = "ONTOLOGY_TEXT_ZH_CN")
	String text_zh_cn;

	@Column(name = "ONTOLOGY_TYPE")
	String type;

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}

	public String getText_en_us() {
		return text_en_us;
	}

	public String getText_zh_cn() {
		return text_zh_cn;
	}

	public String getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setText_en_us(String text_en_us) {
		this.text_en_us = text_en_us;
	}

	public void setText_zh_cn(String text_zh_cn) {
		this.text_zh_cn = text_zh_cn;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "OntologyModel [name=" + name + ", text=" + text + ", type="
				+ type + "]";
	}

}
