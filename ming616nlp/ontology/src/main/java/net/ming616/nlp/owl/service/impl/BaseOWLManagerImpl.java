package net.ming616.nlp.owl.service.impl;

import java.io.File;

import javax.annotation.PostConstruct;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class BaseOWLManagerImpl {

	String owlFileName = "e:/ming616/data/owl/computer.owl";

	String baseURI = "http://nlp.bit.edu.cn/ontologies/";

	File owlFile;

	IRI baseIRI;

	OWLOntologyManager owlManager;

	OWLOntology ontology;

	OWLDataFactory dataFactory;

	@PostConstruct
	void init() {
		this.owlManager = OWLManager.createOWLOntologyManager();
		this.dataFactory = this.owlManager.getOWLDataFactory();
		this.baseIRI = IRI.create(this.baseURI);
		this.owlFile = new File(this.owlFileName);
		try {

			if (this.owlFile.exists()) {
				this.ontology = this.owlManager
						.loadOntologyFromOntologyDocument(this.owlFile);
			} else {
				this.ontology = this.owlManager.createOntology(this.baseIRI);
			}
		} catch (OWLOntologyCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public OWLOntology getOntolgoy() {
		return this.ontology;
	}

}
