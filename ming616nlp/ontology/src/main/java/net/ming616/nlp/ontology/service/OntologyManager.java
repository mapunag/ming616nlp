package net.ming616.nlp.ontology.service;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.ontology.model.OntologyModel;

public interface OntologyManager {

	/**
	 * 根据词文本查找该文本在本体中所有全部数据模型
	 * 
	 * @param wordText
	 * @return
	 */
	public List<OntologyModel> findOntologyList(String text);

	/**
	 * 根据词文本查找该文本在本体中的最可能的数据模型
	 * 
	 * @param wordText
	 * @return
	 */
	public OntologyModel findOntology(String wordText);

	/**
	 * 为句子中每一个可能的词标注本体数据模型
	 * 
	 * @param sentence
	 * @return
	 */
	public List<NLPWord> analyze(List<NLPWord> sentence);

}
