package net.ming616.nlp.ontology.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ontologyEntityRelation")
@Table(name = "T_ONTOLOGY_ENTITY_RELATION")
public class OntologyEntityRelation extends OntologyModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3480546405230992277L;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FIRST_ID")
	OntologyEntity firstEntity;

	@Column(name = "RELATION_NAME")
	String relationName;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SECOND_ID")
	OntologyEntity secondEntity;

	public OntologyEntity getFirstEntity() {
		return firstEntity;
	}

	public String getRelationName() {
		return relationName;
	}

	public OntologyEntity getSecondEntity() {
		return secondEntity;
	}

	public void setFirstEntity(OntologyEntity firstEntity) {
		this.firstEntity = firstEntity;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

	public void setSecondEntity(OntologyEntity secondEntity) {
		this.secondEntity = secondEntity;
	}

}
