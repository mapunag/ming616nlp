package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.EntityDao;
import net.ming616.nlp.ontology.model.OntologyEntity;

import org.springframework.stereotype.Service;

@Service("entityDao")
public class EntityDaoImpl extends GenericDaoImpl<OntologyEntity, Long>
		implements EntityDao {

	public EntityDaoImpl() {
		super(OntologyEntity.class);
	}

}
