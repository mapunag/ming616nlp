package net.ming616.nlp.ontology.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.ontology.dao.EventRelationDao;
import net.ming616.nlp.ontology.model.OntologyEventRelation;
import net.ming616.nlp.ontology.service.EventRelationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("eventRelationManager")
@Transactional
public class EventRelationMangerImpl extends
		GenericManagerImpl<OntologyEventRelation, Long> implements
		EventRelationManager {
	EventRelationDao EventRelationDao;

	@Autowired
	public void setEventRelationDao(EventRelationDao EventRelationDao) {
		this.EventRelationDao = EventRelationDao;
		this.dao = this.EventRelationDao;
	}

}
