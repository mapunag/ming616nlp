package net.ming616.nlp.ontology.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.ontology.model.OntologyModel;
import net.ming616.nlp.ontology.service.AttributeManager;
import net.ming616.nlp.ontology.service.AttributeValueManager;
import net.ming616.nlp.ontology.service.EntityManager;
import net.ming616.nlp.ontology.service.EntityRelationManager;
import net.ming616.nlp.ontology.service.OntologyManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ontologyManager")
@Transactional
public class OntologyManagerImpl implements OntologyManager {
	@Autowired
	AttributeManager attributeManager;
	@Autowired
	AttributeValueManager attributeValueManager;

	@Autowired
	EntityManager entityManager;

	@Autowired
	EntityRelationManager entityRelationManager;

	Map<String, OntologyModel> ontoMap = new HashMap<String, OntologyModel>();

	String separator = ":";

	public List<NLPWord> analyze(List<NLPWord> sentence) {
		// for (NLPWord nlpWord : sentence) {
		// String wordText = nlpWord.getText();
		// OntologyModel onto = this.findOntology(wordText);
		// nlpWord.setOntologyModel(onto);
		// }
		return sentence;
	}

	/**
	 * find onto by word text,if it has more than one, return the first;
	 */
	public OntologyModel findOntology(String wordText) {
		OntologyModel result = null;
		List<OntologyModel> results = this.findOntologyList(wordText);
		if (null != results && results.size() > 0) {
			result = results.get(0);
		}
		return result;
	}

	public List<OntologyModel> findOntologyList(String wordText) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("text", "[\"" + wordText + "\"]");
		List<OntologyModel> results = new ArrayList<OntologyModel>();
		results.addAll(this.entityManager.findByNamedQuery("entityQueryByText",
				queryParams));
		results.addAll(this.attributeManager.findByNamedQuery(
				"attributeQueryByText", queryParams));
		results.addAll(this.attributeValueManager.findByNamedQuery(
				"attributeValueQueryByText", queryParams));
		return results;
	}

}
