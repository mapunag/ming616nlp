package net.ming616.nlp.ontology.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.ontology.model.OntologyEventArgumentValue;

public interface ArgumentValueManager extends
		GenericManager<OntologyEventArgumentValue, Long> {
}
