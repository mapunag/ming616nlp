package net.ming616.nlp.ontology.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.ontology.dao.EventDao;
import net.ming616.nlp.ontology.model.OntologyEvent;

import org.springframework.stereotype.Service;

@Service("eventDao")
public class EventDaoImpl extends GenericDaoImpl<OntologyEvent, Long> implements
		EventDao {

	public EventDaoImpl() {
		super(OntologyEvent.class);
	}

}
