package net.ming616.nlp.ontology.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "ontologyEventArgument")
@Table(name = "T_ONTOLOGY_EVENT_ARGUMENT")
@NamedQueries({
		@NamedQuery(name = "argumentQueryByName", query = "SELECT OBJECT(onto) FROM ontologyEventArgument onto WHERE onto.name = :name"),
		@NamedQuery(name = "argumentQueryByText", query = "SELECT OBJECT(onto) FROM ontologyEventArgument onto WHERE onto.text = :text") })
public class OntologyEventArgument extends OntologyModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5639701598575887985L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "EVENT_ID")
	OntologyEvent event;

	@OneToMany(mappedBy = "argument", fetch = FetchType.LAZY)
	Set<OntologyEventArgumentValue> values = new HashSet<OntologyEventArgumentValue>();

	public OntologyEvent getEvent() {
		return event;
	}

	public void setEvent(OntologyEvent event) {
		this.event = event;
	}

	public Set<OntologyEventArgumentValue> getValues() {
		return values;
	}

	public void setValues(Set<OntologyEventArgumentValue> values) {
		this.values = values;
	}

}
