package net.ming616.nlp.qa.question.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import net.ming616.nlp.base.model.BaseTreeModel;

@Table(name = "T_CATEGORY")
@Entity(name = "category")
public class Category extends BaseTreeModel<Category> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3614785534038940700L;

	@Column(name = "NAME")
	String name;

	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
	@OrderBy("sort asc")
	List<Question> questions;

	@Column(name = "URL")
	String url;

	public void addQuestion(Question question) {
		if (this.questions == null) {
			this.questions = new ArrayList<Question>();
		}
		this.questions.add(question);
	}

	public String getName() {
		return name;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public String getUrl() {
		return url;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
