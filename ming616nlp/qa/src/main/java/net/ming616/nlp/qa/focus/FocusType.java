package net.ming616.nlp.qa.focus;

public enum FocusType {
	SPECIAL, ALTERNATIVE, YES_NO, POS_NEG;
}