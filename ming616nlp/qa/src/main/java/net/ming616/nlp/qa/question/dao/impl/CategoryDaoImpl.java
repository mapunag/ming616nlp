package net.ming616.nlp.qa.question.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericTreeDaoImpl;
import net.ming616.nlp.qa.question.dao.CategoryDao;
import net.ming616.nlp.qa.question.model.Category;

import org.springframework.stereotype.Service;

@Service(value = "categoryDao")
public class CategoryDaoImpl extends GenericTreeDaoImpl<Category, Long>
		implements CategoryDao {
	public CategoryDaoImpl() {
		super(Category.class);
	}

}
