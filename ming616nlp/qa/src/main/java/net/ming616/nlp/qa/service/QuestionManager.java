package net.ming616.nlp.qa.service;

import net.ming616.nlp.base.service.GenericManager;
import net.ming616.nlp.qa.question.model.Question;

public interface QuestionManager extends GenericManager<Question, Long> {

}
