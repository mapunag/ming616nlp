package net.ming616.nlp.qa.question.dao.impl;

import net.ming616.nlp.base.dao.impl.GenericDaoImpl;
import net.ming616.nlp.qa.question.dao.QuestionDao;
import net.ming616.nlp.qa.question.model.Question;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service(value = "quesitonDao")
public class QuestionDaoImpl extends GenericDaoImpl<Question, Long> implements
		QuestionDao {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(QuestionDaoImpl.class);

	public QuestionDaoImpl() {
		super(Question.class);
	}


}
