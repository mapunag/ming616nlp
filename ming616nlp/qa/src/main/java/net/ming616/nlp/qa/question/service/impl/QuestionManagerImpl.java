package net.ming616.nlp.qa.question.service.impl;

import net.ming616.nlp.base.service.impl.GenericManagerImpl;
import net.ming616.nlp.qa.question.dao.QuestionDao;
import net.ming616.nlp.qa.question.model.Question;
import net.ming616.nlp.qa.service.QuestionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "questionManager")
@Transactional
public class QuestionManagerImpl extends GenericManagerImpl<Question, Long>
		implements QuestionManager {
	QuestionDao questionDao;

	@Autowired
	public QuestionManagerImpl(QuestionDao questionDao) {
		super(questionDao);
		this.questionDao = questionDao;
	}

}
