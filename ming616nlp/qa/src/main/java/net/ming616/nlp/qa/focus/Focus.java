package net.ming616.nlp.qa.focus;

import net.ming616.nlp.qa.model.QABaseModel;

/**
 * 
 * @author liuxiaoming
 * 
 */
public class Focus extends QABaseModel {

	FocusType focusType;
	TargetType targetType;
	String focusText;
	String targetText;

	public FocusType getFocusType() {
		return focusType;
	}

	public void setFocusType(FocusType focusType) {
		this.focusType = focusType;
	}

	public TargetType getTargetType() {
		return targetType;
	}

	public void setTargetType(TargetType targetType) {
		this.targetType = targetType;
	}

	public String getFocusText() {
		return focusText;
	}

	public void setFocusText(String focusText) {
		this.focusText = focusText;
	}

	public String getTargetText() {
		return targetText;
	}

	public void setTargetText(String targetText) {
		this.targetText = targetText;
	}

}
