package net.ming616.nlp.qa.question.service;

import net.ming616.nlp.base.service.GenericTreeManager;
import net.ming616.nlp.qa.question.model.Category;

public interface CategoryManager extends GenericTreeManager<Category, Long> {

}
