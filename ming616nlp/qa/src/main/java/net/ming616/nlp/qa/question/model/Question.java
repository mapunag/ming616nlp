package net.ming616.nlp.qa.question.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import net.ming616.nlp.base.model.BaseModel;



@Table(name = "T_QUESTION")
@Entity(name = "question")
@NamedQueries({ @NamedQuery(name = "questionQueryByTitle", query = "SELECT OBJECT(o) FROM question o WHERE o.title = :title") })
public class Question extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3621170443768915301L;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CATEGORY_ID")
	Category category;

	/**
	 */
	@Lob
	@Column(name = "CONTENT")
	String content;

	/**
	 */
	@Lob
	@Column(name = "LTP_RESULT")
	String ltpResult;

	/**
	 */
	@Lob
	@Column(name = "OFFICE_ANSWER")
	String officeAnswer;

	/**
	 */
	@Lob
	@Column(name = "STAR_ANSWER")
	String starAnswer;

	/**
	 */
	@Column(name = "TITLE")
	String title;

	@Column(name = "URL")
	String url;

	public Category getCategory() {
		return category;
	}

	public String getContent() {
		return content;
	}

	public String getLtpResult() {
		return ltpResult;
	}

	public String getOfficeAnswer() {
		return officeAnswer;
	}

	public String getStarAnswer() {
		return starAnswer;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setLtpResult(String ltpResult) {
		this.ltpResult = ltpResult;
	}

	public void setOfficeAnswer(String officeAnswer) {
		this.officeAnswer = officeAnswer;
	}

	public void setStarAnswer(String starAnswer) {
		this.starAnswer = starAnswer;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
