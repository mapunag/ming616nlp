package net.ming616.nlp.qid;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.qid.model.QIDGraph;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

@Component
public class QIDService {

	/**
	 * whether q contains p or not;
	 * 
	 * @param q
	 * @param p
	 * @return
	 */
	boolean contain(QIDGraph q, QIDGraph p) {
		boolean result = false;
		if (CollectionUtils.containsAny(q.getVertexList(), p.getVertexList())
				&& CollectionUtils
						.containsAny(q.getEdgeList(), p.getEdgeList())) {
			result = true;
		}
		return result;
	}

	/**
	 * Generating graph for question
	 * 
	 * @param words
	 * @return
	 */
	public QIDGraph generate(List<NLPWord> words) {
		return null;
	}

}
