package net.ming616.nlp.qid.model;

import java.util.ArrayList;
import java.util.List;

public class QIDGraph {

	List<QIDEdge> edgeList = new ArrayList<QIDEdge>();

	public List<QIDEdge> getEdgeList() {
		return edgeList;
	}

	public void setEdgeList(List<QIDEdge> edgeList) {
		this.edgeList = edgeList;
	}

	public List<QIDVertex> getVertexList() {
		return vertexList;
	}

	public void setVertexList(List<QIDVertex> vertexList) {
		this.vertexList = vertexList;
	}

	List<QIDVertex> vertexList = new ArrayList<QIDVertex>();

	public void addEdge(QIDEdge e) {
		this.edgeList.add(e);

	}

	public void addVertex(QIDVertex v) {
		this.vertexList.add(v);
	}
}
