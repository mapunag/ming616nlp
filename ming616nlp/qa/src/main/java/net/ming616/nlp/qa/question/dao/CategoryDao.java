package net.ming616.nlp.qa.question.dao;

import net.ming616.nlp.base.dao.GenericTreeDao;
import net.ming616.nlp.qa.question.model.Category;

public interface CategoryDao extends GenericTreeDao<Category, Long> {

}
