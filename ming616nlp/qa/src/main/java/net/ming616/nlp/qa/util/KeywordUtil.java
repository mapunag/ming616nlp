package net.ming616.nlp.qa.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class KeywordUtil {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(KeywordUtil.class);

	// Comparative Word Map
	private static Map<String, String> comparativeWordMap;

	// Comparative Result Word Map
	private static Map<String, String> comparativeResultWordMap;

	// Interrogative Word Map
	private static Map<String, String> interrogativeWordMap;

	public static Map<String, String> getComparativeWordMap() {
		if (null == KeywordUtil.comparativeWordMap) {
			KeywordUtil.comparativeWordMap = new HashMap<String, String>();
			String path = "E:/ming616/data/comparative/Comparative_Word.txt";
			List<String> lines = KeywordUtil.getWordList(path);
			for (String word : lines) {
				KeywordUtil.comparativeWordMap.put(word, null);
			}
		}
		return KeywordUtil.comparativeWordMap;
	}

	public static Map<String, String> getComparativeResultWordMap() {
		if (null == KeywordUtil.comparativeResultWordMap) {
			KeywordUtil.comparativeResultWordMap = new HashMap<String, String>();
			String path = "E:/ming616/data/comparative/Comparative_Result_Word.txt";
			List<String> lines = KeywordUtil.getWordList(path);
			for (String word : lines) {
				KeywordUtil.comparativeResultWordMap.put(word, null);
			}
		}
		return KeywordUtil.comparativeResultWordMap;
	}

	public static Map<String, String> getInterrogativeWord() {
		if (null == KeywordUtil.interrogativeWordMap) {
			KeywordUtil.interrogativeWordMap = new HashMap<String, String>();
			String path = "E:/ming616/data/Interrogative_Word.txt";
			List<String> lines = KeywordUtil.getWordList(path);
			for (String word : lines) {
				KeywordUtil.interrogativeWordMap.put(word, null);
			}
		}
		return KeywordUtil.interrogativeWordMap;
	}

	private static List<String> getWordList(String filePath) {
		// InputStream input = KeywordUtil.class.getResourceAsStream(filePath);
		List<String> lines = null;
		try {
			lines = FileUtils.readLines(new File(filePath));
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return lines;
	}

	public static void main(String[] args) {
//		Map<String, String> map = KeywordUtil.getComparativeWordMap();
		Map<String, String> map = KeywordUtil.getInterrogativeWord();
//		Map<String, String> map = KeywordUtil.getComparativeResultWordMap();
		for (Entry<String, String> entry : map.entrySet()) {
			if (logger.isInfoEnabled()) {
				logger.info("entry=" + entry); //$NON-NLS-1$
			}
		}
	}
}
