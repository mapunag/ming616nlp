package net.ming616.nlp.qa.question.dao;

import net.ming616.nlp.base.dao.GenericDao;
import net.ming616.nlp.qa.question.model.Question;

public interface QuestionDao extends GenericDao<Question, Long> {


}
