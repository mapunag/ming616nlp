package net.ming616.nlp.mallet;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

public class Mallet {

	private static String mallet_home = "D:/ming616/mallet-2.0.7/";

	public static String importData(String dataFile, String outFile) {
		String[] cmd = { mallet_home + "bin/mallet.bat", "import-file",
				"--input", dataFile, "--output", outFile };
		String result = null;
		try {
			Process process = Runtime.getRuntime().exec(cmd);
			InputStream inputStream = process.getInputStream();
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer, "GBK");
			result = writer.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) {
		String dataFile = "d:/ming616/experiment/hit/train.txt";
		String outFile = "d:/ming616/experiment/hit/train.mallet";
		Mallet.importData(dataFile, outFile);
		System.exit(0);
	}
}
