package net.ming616.nlp.qa.question.service.impl;

import net.ming616.nlp.base.service.impl.GenericTreeManagerImpl;
import net.ming616.nlp.qa.question.dao.CategoryDao;
import net.ming616.nlp.qa.question.model.Category;
import net.ming616.nlp.qa.question.service.CategoryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "categoryManager")
@Transactional
public class CategoryManagerImpl extends GenericTreeManagerImpl<Category, Long>
		implements CategoryManager {
	CategoryDao categoryDao;

	@Autowired
	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
		this.dao = this.categoryDao;
	}

}
