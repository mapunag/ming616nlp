package net.ming616.nlp.qa.service;

import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.qa.question.model.Question;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-qa.xml" })
public class QuestionTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(QuestionTest.class);

	@Autowired
	QuestionManager questionManager;

	List<Question> questions = null;

	@Before
	public void init() {
		this.questions = this.questionManager.getAll();
	}

	@Test
	public void testPattern() {
		for (Question q : this.questions) {
			logger.info(q);
		}
	}

	boolean isSpecail(String q) {
		boolean result = false;
		
		return result;
	}

}
