package net.ming616.nlp.qa.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.qa.focus.Focus;
import net.ming616.nlp.qa.util.QuestionUtils;
import net.sf.json.JSONObject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

public class HITTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(HITTest.class);

	String hitDir = "D:/ming616/experiment/hit/";

	String trainFileString = this.hitDir + "trainquestion_forXML_20060717.xml";

	String testFileString = this.hitDir + "testquestion_forXML_20060717.xml";

	String outFileString = this.hitDir + "out.xls";

	String dictString = this.hitDir + "dict.txt";

	Map<String, Integer> wordMap = new HashMap<String, Integer>();

	private void outputDict() throws IOException {
		OutputStream out = new FileOutputStream(this.dictString, true);
		for (Entry<String, Integer> e : this.wordMap.entrySet()) {
			IOUtils.write(e.getKey() + "=" + e.getValue() + "\n", out);
		}
	}

	// private void generateDict() {
	//
	// }
	//
	// private void initWordMap() throws FileNotFoundException, IOException {
	// if (CollectionUtils.isEmpty(this.wordMap)) {
	// List<String> lines = IOUtils.readLines(new FileInputStream(
	// this.dictString));
	// for (String line : lines) {
	// String key = StringUtils.substringBeforeLast(line, "=");
	// String value = StringUtils.substringAfterLast(line, "=");
	// this.wordMap.put(key, Integer.valueOf(value));
	// }
	// }
	// }

	private void ToExcel(String inputFileName, String outExcelFileName,
			String sheetName) throws IOException {
		File file = new File(inputFileName);
		Workbook wb = new HSSFWorkbook(new FileInputStream(outExcelFileName));
		int index = wb.getSheetIndex(sheetName);
		try {
			wb.removeSheetAt(index);
		} catch (Exception e) {
			logger.info(sheetName + "is not exist");
		}
		Sheet sheet = wb.createSheet(sheetName);
		List<Map<String, String>> qs = this.parseHIT(file);
		for (Map<String, String> map : qs) {
			List<String> line = new ArrayList<String>();
			String id = map.get("id");
			line.add(map.get("id"));
			// line.add(map.get("cont"));
			line.add(map.get("coarseTag"));
			line.add(map.get("fineTag"));
			line.add(map.get("focus"));
			// line.add(parseString);
			Row row = sheet.createRow(Integer.valueOf(id));

			int j = 0;
			for (String v : line) {
				Cell c = row.createCell(j++);
				c.setCellValue(v);
			}
		}
		// return new HSSFWorkbook(new FileInputStream(filename));
		FileOutputStream fileOut = new FileOutputStream(outExcelFileName);
		wb.write(fileOut);
		fileOut.close();
	}

	private void ToMallet(String inputFileName, String outMalletFileName,
			boolean isTest) throws IOException {
		File file = new File(inputFileName);
		List<Map<String, String>> qs = this.parseHIT(file);
		OutputStream out = new FileOutputStream(outMalletFileName);
		List<String> lines = new ArrayList<String>();
		for (Map<String, String> map : qs) {
			StringBuffer buffer = new StringBuffer();
			// buffer.append(map.get("id") + "\t");
			// buffer.append(map.get("cont") + "\t");
			// String coarseTag = map.get("coarseTag");
			// buffer.append(coarseTag + "\t");
			// if (StringUtils.startsWith(coarseTag, "TIME")) {
			buffer.append(map.get("fineTag") + "\t");
			String focusJSON = map.get("focus");
			JSONObject f = JSONObject.fromObject(focusJSON);

			String focusText = f.getString("focusText");
			// buffer.append("focusText:" + this.wordMap.get(focusText) +
			// "\t");

			// buffer.append("focusType:" + f.getString("focusType") +
			// "\t");

			String targetText = f.getString("targetText");
			// buffer.append("targetText:" + this.wordMap.get(targetText) +
			// "\t");
			buffer.append(focusText + "_");
			buffer.append(targetText + "\t");

			// buffer.append("targetType:" + f.getString("targetType") +
			// "\t");

			lines.add(buffer.toString());
		}
		// }
		IOUtils.writeLines(lines, "\n", out);
		IOUtils.closeQuietly(out);
	}

	@Test
	public void testTrain() throws IOException {
		this.ToExcel(this.trainFileString, this.outFileString, "哈工大训练问题集合");
		this.ToExcel(this.testFileString, this.outFileString, "哈工大测试问题集合");
		// this.initWordMap();
		// this.ToMallet(this.trainFileString, this.hitDir + "train.txt",
		// false);
		// this.ToMallet(this.testFileString, this.hitDir + "test.txt", true);
	}

	private List<Map<String, String>> parseHIT(File file) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		Document doc = null;
		try {
			doc = Jsoup.parse(file, "UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Elements sents = doc.select("para sent");
		int i = 1;
		for (Element sent : sents) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", String.valueOf(i++));

			String cont = sent.attr("cont");
			map.put("cont", cont);

			String fineTag = sent.attr("tag");
			map.put("fineTag", fineTag);

			String coarseTag = StringUtils.substringBefore(fineTag, "_");
			map.put("coarseTag", coarseTag);

			String parseString = sent.toString();
			map.put("parseString", parseString);

			List<NLPWord> words = this.convert(sent.select("word"));
			Focus focus = QuestionUtils.getFocus(words);
			map.put("focus", focus.toString());

			result.add(map);
		}
		return result;
	}

	private List<NLPWord> convert(Elements words) {
		List<NLPWord> result = new ArrayList<NLPWord>();
		for (Element e : words) {
			NLPWord w = new NLPWord();
			w.setId(Integer.valueOf(e.attr("id")));
			String wordText = e.attr("cont");

			// if (!this.wordMap.containsValue(wordText)) {
			// Integer key = this.wordMap.size() + 1;
			// this.wordMap.put(wordText, key);
			// }

			w.setText(wordText);
			w.setPos(e.attr("pos"));
			w.setParent(Integer.valueOf(e.attr("parent")));
			result.add(w);
		}
		return result;
	}
}
