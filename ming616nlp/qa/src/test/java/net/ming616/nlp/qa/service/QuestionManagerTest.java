package net.ming616.nlp.qa.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.base.utils.Page;
import net.ming616.nlp.qa.question.model.Question;
import net.ming616.nlp.qa.util.KeywordUtil;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-qa.xml" })
public class QuestionManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(QuestionManagerTest.class);

	@Autowired
	QuestionManager questionManager;

	List<Question> questions = null;

	@Before
	public void init() {
		// this.questions = this.questionManager.getAll();
	}

	// @Test
	public void testSave() {
		for (int i = 0; i < 100; i++) {
			Question q = new Question();
			q.setContent("content " + i);
			q.setOfficeAnswer("office answer" + i);
			q.setStarAnswer("star answer " + i);
			this.questionManager.save(q);
		}
	}

	// @Test
	public void testGet() {
		try {
			Question result = this.questionManager.get(Long
					.valueOf("2010092411213805630"));
			if (logger.isInfoEnabled()) {
				logger.info("testGet() - Question result=" + result); //$NON-NLS-1$
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// @Test
	public void testQuery() {
		Question q = new Question();
		q.setTitle("���������ʾ����ô����");
		Page<Question> page = this.questionManager
				.find(q, new Page<Question>());
		if (logger.isInfoEnabled()) {
			logger.info("page=" + page); //$NON-NLS-1$
		}
	}

	@Test
	public void getComparativeQuestions() {
		List<Question> list = this.questionManager.getAllDistinct();
		OutputStream out = null;
		try {
			out = new FileOutputStream(
					"e:/ming616/data/comparative/comparative_question.txt",
					true);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Set<String> comparatives = new HashSet<String>();

		for (Question question : list) {
			String title = question.getTitle();
			if (this.contain(title, KeywordUtil.getComparativeWordMap())
					|| this.contain(title,
							KeywordUtil.getComparativeResultWordMap())) {
				comparatives.add(title);
			}
		}

		try {
			for (String s : comparatives) {
				IOUtils.write(s + "\n", out);
			}

		} catch (IOException e) {
			logger.error(e.getMessage());
		}

	}

	private boolean contain(String sentence, Map<String, String> map) {
		for (Entry<String, String> entry : map.entrySet()) {
			if (StringUtils.contains(sentence, entry.getKey())) {
				return true;
			}
		}
		return false;
	}

	// @Test
	public void getInterrogativeQuestion() {
		List<Question> list = this.questionManager.getAll();
		OutputStream out = null;
		try {
			out = new FileOutputStream(
					"e:/ming616/data/interrogative_question.txt", true);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for (Question question : list) {
			String title = question.getTitle();
			if (this.contain(title, KeywordUtil.getInterrogativeWord())) {
				try {
					IOUtils.write(title + "\n", out, null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
