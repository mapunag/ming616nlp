package net.ming616.nlp.base.service;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations = {
		"classpath:applicationContextTest-resource.xml",
		"classpath:applicationContext-dao.xml",
		"classpath:applicationContext-base.xml" })
public class BaseManagerTestCase extends
		AbstractTransactionalJUnit4SpringContextTests {
	@Test
	@Ignore
	public void test() {
		Assert.assertEquals(true, true);
	}
}
