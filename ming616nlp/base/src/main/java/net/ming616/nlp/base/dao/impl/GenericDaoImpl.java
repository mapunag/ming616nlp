package net.ming616.nlp.base.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import net.ming616.nlp.base.dao.GenericDao;
import net.ming616.nlp.base.dao.utils.PKgen;
import net.ming616.nlp.base.model.BaseModel;
import net.ming616.nlp.base.utils.Page;
import net.ming616.nlp.base.utils.ReflectionUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * This class serves as the Base class for all other DAOs - namely to hold
 * common CRUD methods that they might all use. You should only need to extend
 * this class when your require custom CRUD logic.
 * <p/>
 * <p>
 * To register this class in your Spring context file, use the following XML.
 * 
 * <pre>
 *      &lt;bean id="fooDao" class="org.appfuse.dao.hibernate.GenericDaoHibernate"&gt;
 *          &lt;constructor-arg value="org.appfuse.model.Foo"/&gt;
 *      &lt;/bean&gt;
 * </pre>
 * 
 * @author <a href="mailto:bwnoll@gmail.com">Bryan Noll</a>
 * @param <T>
 *            a type variable
 * @param <PK>
 *            the primary key for that type
 */
public class GenericDaoImpl<T extends BaseModel, PK extends Serializable>
		implements GenericDao<T, PK> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(GenericDaoImpl.class);

	/**
	 * Log variable for all child classes. Uses LogFactory.getLog(getClass())
	 * from Commons Logging
	 */
	protected final Log log = LogFactory.getLog(getClass());
	protected Class<T> persistentClass;
	private HibernateTemplate hibernateTemplate;
	private SessionFactory sessionFactory;

	/**
	 * Constructor that takes in a class to see which type of entity to persist.
	 * Use this constructor when subclassing.
	 * 
	 * @param persistentClass
	 *            the class type you'd like to persist
	 */
	public GenericDaoImpl(final Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	/**
	 * Constructor that takes in a class and sessionFactory for easy creation of
	 * DAO.
	 * 
	 * @param persistentClass
	 *            the class type you'd like to persist
	 * @param sessionFactory
	 *            the pre-configured Hibernate SessionFactory
	 */
	public GenericDaoImpl(final Class<T> persistentClass,
			SessionFactory sessionFactory) {
		this.persistentClass = persistentClass;
		this.sessionFactory = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	public HibernateTemplate getHibernateTemplate() {
		return this.hibernateTemplate;
	}

	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	@Autowired
	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> getAll() {
		return hibernateTemplate.loadAll(this.persistentClass);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> getAllDistinct() {
		Collection<T> result = new LinkedHashSet<T>(getAll());
		return new ArrayList<T>(result);
	}

	/**
	 * {@inheritDoc}
	 */
	public T get(PK id) {
		T entity = (T) hibernateTemplate.get(this.persistentClass, id);

		if (entity == null) {
			log.warn("Uh oh, '" + this.persistentClass + "' object with id '"
					+ id + "' not found...");
			throw new ObjectRetrievalFailureException(this.persistentClass, id);
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean exists(PK id) {
		T entity = (T) hibernateTemplate.get(this.persistentClass, id);
		return entity != null;
	}

	/**
	 * {@inheritDoc}
	 */
	public T save(T object) {
		if (null == object.getId()) {
			object.setId(PKgen.getInstance().nextPK());
		} else {
			object.setDateModified(new Date());
		}
		return (T) hibernateTemplate.merge(object);
	}

	public void saveAll(List<T> list) {
		for (T t : list) {
			if (null == t.getId()) {
				t.setId(PKgen.getInstance().nextPK());
			} else {
				t.setDateModified(new Date());
			}
		}
		this.hibernateTemplate.saveOrUpdateAll(list);
	}

	/**
	 * {@inheritDoc}
	 */
	public void remove(PK id) {
		hibernateTemplate.delete(this.get(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByNamedQuery(String queryName,
			Map<String, Object> queryParams) {
		String[] params = new String[queryParams.size()];
		Object[] values = new Object[queryParams.size()];

		int index = 0;
		for (String s : queryParams.keySet()) {
			params[index] = s;
			values[index++] = queryParams.get(s);
		}

		return hibernateTemplate.findByNamedQueryAndNamedParam(queryName,
				params, values);
	}

	@SuppressWarnings("unchecked")
	public Page<T> findPage(Page<T> page) {
		if (null == page) {
			page = new Page<T>();
		}
		final int start = page.getStartRowNumber();
		final int end = page.getStartRowNumber() + page.getLength();
		final String hql = "from " + this.persistentClass.getName();
		String countHql = "select count(id) " + hql;
		// ��ȡHibernate��ǰ��Session
		Session session = hibernateTemplate.getSessionFactory()
				.getCurrentSession();
		// ��ȡ��¼����
		Long rowCount = (Long) session.createQuery(countHql).uniqueResult();
		// ��ȡList
		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(end);
		List<T> list = query.list();

		page.setRowCount(rowCount.intValue());
		page.setRowNumber(page.getStartRowNumber() + list.size());
		page.setList(list);
		return page;
	}

	@SuppressWarnings("unchecked")
	public Page<T> find(T model, Page<T> page) {
		if (null == page) {
			page = new Page<T>();
		}
		List<Field> fields = ReflectionUtils
				.getDeclaredField(this.persistentClass);
		StringBuffer buffer = new StringBuffer(" where ");
		for (int i = 0; i < fields.size(); i++) {
			Field field = fields.get(i);
			String fieldName = field.getName();
			if (StringUtils.equalsIgnoreCase(fieldName, "serialVersionUID")) {
				continue;
			} else {
				Object value = ReflectionUtils.getFieldValue(model, fieldName);
				if (null != value) {
					if (value instanceof String) {
						String valueString = String.valueOf(value);
						if (StringUtils.isNotBlank(valueString)) {
							buffer.append(fieldName + "=" + "\'" + valueString
									+ "\'");
							if (i != fields.size() - 1) {
								buffer.append(" and ");
							}
						}
					} else if (value instanceof Long) {
						String valueString = String.valueOf(value);
						if (StringUtils.isNotBlank(valueString)) {
							buffer.append(fieldName + "=" + valueString);
							if (i != fields.size() - 1) {
								buffer.append(" and ");
							}
						}
					}
				}
			}
		}

		final int start = page.getStartRowNumber();
		final int end = page.getStartRowNumber() + page.getLength();
		final String hql = "from " + this.persistentClass.getName()
				+ buffer.toString();
		if (logger.isInfoEnabled()) {
			logger.info("find(T, Page<T>) - String hql=" + hql); //$NON-NLS-1$
		}

		String countHql = "select count(id) " + hql;
		Session session = hibernateTemplate.getSessionFactory()
				.getCurrentSession();
		Long rowCount = (Long) session.createQuery(countHql).uniqueResult();
		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(end);
		List<T> list = query.list();

		if (null == rowCount) {
			rowCount = Long.valueOf(0);
		}
		page.setRowCount(rowCount.intValue());
		page.setRowNumber(page.getStartRowNumber() + list.size());
		page.setList(list);

		return page;
	}

	public Boolean exist(T model) {
		Page<T> page = new Page<T>();
		page.setLength(1);
		page = this.find(model, page);
		List<T> list = page.getList();
		if (null != list && list.size() > 0) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, String propertyValue) {
		String hql = "From " + this.persistentClass + " where " + propertyName
				+ "=?";
		List<T> list = this.hibernateTemplate.find(hql, propertyValue);
		return list;
	}
}
