package net.ming616.nlp.base.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import antlr.collections.List;

/**
 * 
 * @author liuxiaoming
 * 
 */
@MappedSuperclass
public class BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9076391593406526326L;

	/**
	 */
	@Column(name = "DATECREATED")
	protected Date dateCreated = new Date();

	/**
	 */
	@Column(name = "DATEMODIFED")
	protected Date dateModified;

	/**
	 */
	@Column(name = "DELETED")
	protected Boolean deleted = false;

	/**
	 */
	@Id
	@Column(name = "ID")
	Long id;

	/**
	 */
	@Column(name = "SORT", nullable = false)
	protected Long sort = Long.valueOf(0);

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseModel other = (BaseModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig
				.setExcludes(new String[] { "children", "parent", "questions" });
		jsonConfig.setEnclosedType(List.class);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.STRICT);
		return JSONObject.fromObject(this, jsonConfig).toString();
	}

}
