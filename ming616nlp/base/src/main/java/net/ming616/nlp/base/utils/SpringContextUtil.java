package net.ming616.nlp.base.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringContextUtil {
	private static ApplicationContext context;

	private SpringContextUtil() {
	}

	static String[] xmls = new String[] { "applicationContext*.xml" };

	public static ApplicationContext getContext() {
		if (context == null) {
			context = new ClassPathXmlApplicationContext(xmls);
		}
		return context;
	}
}