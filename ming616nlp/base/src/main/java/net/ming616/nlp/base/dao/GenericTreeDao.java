package net.ming616.nlp.base.dao;

import java.io.Serializable;
import java.util.List;

import net.ming616.nlp.base.model.BaseTreeModel;

public interface GenericTreeDao<T extends BaseTreeModel<?>, PK extends Serializable>
		extends GenericDao<T, PK> {

	List<T> getAllLeaves();

}
