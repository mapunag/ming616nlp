package net.ming616.nlp.base.utils;

import java.util.List;

/**
 * 分页工具类
 * 
 * @author ming616
 * 
 */
@SuppressWarnings("serial")
public class Page<T> implements java.io.Serializable {
	private int length = 20;// 结果集合的长度,即每一页最大行数,默认值为20
	private List<T> list;
	private int pageCount;// 总页数

	private int pageNumber;// 当前页码数

	private int rowCount;// 总记录数
	private int rowNumber;// 当前记录数
	private int startRowNumber;// 起始行号

	public Page() {
		this.startRowNumber = 0;
	}

	/**
	 * startRowNumber // 起始记录号 length //每一页的最大记录数
	 * 
	 * @param startRowNumber
	 * @param length
	 */
	public Page(Integer startRowNumber, Integer length) {
		if (null == startRowNumber) {
			this.startRowNumber = 0;
		} else {
			this.startRowNumber = startRowNumber;
		}
		if (null == length) {
			this.setLength(0);
		} else {
			this.setLength(length);
		}

	}

	public int getLength() {
		return length;
	}

	public List<T> getList() {
		return list;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getRowCount() {
		return rowCount;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public int getStartRowNumber() {
		return startRowNumber;
	}

	public void nextPage() {
		this.setPageNumber(this.pageNumber++);
	}

	public void previousPage() {
		this.setPageNumber(this.pageNumber--);
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
		if (this.pageNumber <= 0) {
			this.startRowNumber = 1;
		} else {
			this.startRowNumber = (this.pageNumber - 1) * this.length + 1;
		}
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
		if (this.rowCount == 0) {
			this.pageCount = 0;
		} else {
			int remainder = this.rowCount % this.length;
			if (remainder == 0) {
				this.pageCount = this.rowCount / this.length;
			} else {
				this.pageCount = this.rowCount / this.length + 1;
			}
		}
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
		if (this.rowNumber == 0) {
			this.pageNumber = 0;
		} else {
			int remainder = this.rowNumber % this.length;
			if (remainder == 0) {
				this.pageNumber = this.rowNumber / this.length;
			} else {
				this.pageNumber = this.rowNumber / this.length + 1;
			}
		}
	}

	public void setStartRowNumber(int startRowNumber) {
		this.startRowNumber = startRowNumber;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer(200);
		buffer.append("\n分页信息：[\n");
		buffer.append("\t每页最大记录数：\t" + this.getLength() + "条\n");
		buffer.append("\t总记录数：   \t共" + this.getRowCount() + "条\n");
		buffer.append("\t总页数：     \t共" + this.getPageCount() + "页\n");
		buffer.append("\t当前记录数：\t 第" + this.getRowNumber() + "条\n");
		buffer.append("\t当前页数：  \t 第" + this.getPageNumber() + "页");
		buffer.append("\n]");
		return buffer.toString();
	}

}
