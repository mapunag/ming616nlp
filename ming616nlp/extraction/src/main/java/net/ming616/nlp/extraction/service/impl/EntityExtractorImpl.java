package net.ming616.nlp.extraction.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.extraction.service.EntityExtractor;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

@Service("entityExtractor")
public class EntityExtractorImpl implements EntityExtractor {

	List<String> entityWords;
	List<String> attributeWords;
	List<String> attributeValueWords;

	@PostConstruct
	public void init() {
		if (this.entityWords == null) {
			this.entityWords = this
					.loadWords("e:/ming616/data/entity/entity_words.txt");
			this.attributeWords = this
					.loadWords("e:/ming616/data/entity/attribute_words.txt");
			this.attributeValueWords = this
					.loadWords("e:/ming616/data/entity/attribute_value_words.txt");
		}
	}

	private List<String> loadWords(String fileName) {
		List<String> words = null;
		try {
			words = FileUtils.readLines(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return words;
	}

	public List<NLPWord> extract(List<NLPWord> sentence) {
		for (NLPWord nlpWord : sentence) {
			if (this.entityWords.contains(nlpWord.getText())) {
				nlpWord.setEntityLabel("B-Entity");
			} else if (this.attributeWords.contains(nlpWord.getText())) {
				nlpWord.setEntityLabel("B-Attribute");
			} else if (this.attributeValueWords.contains(nlpWord.getText())) {
				nlpWord.setEntityLabel("B-AttributeValue");
			}
		}
		return sentence;
	}

}
