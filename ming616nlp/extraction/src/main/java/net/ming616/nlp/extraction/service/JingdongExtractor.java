package net.ming616.nlp.extraction.service;

import java.util.Map;

import net.ming616.nlp.extraction.model.JingdongProduct;

/**
 * product extractor for http://www.360buy.com
 * 
 * @author liuxiaoming
 * 
 */
public interface JingdongExtractor {

	Map<String, String> getProductURLList(String baseURL);

	JingdongProduct getProduct(String productURL);

}
