package net.ming616.nlp.extraction.service;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

public interface EventExtractor {


	List<NLPWord> extract(List<NLPWord> sentence);
}
