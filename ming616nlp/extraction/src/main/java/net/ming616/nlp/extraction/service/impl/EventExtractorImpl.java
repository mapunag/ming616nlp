package net.ming616.nlp.extraction.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.extraction.service.EventExtractor;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

@Service("eventExtractor")
public class EventExtractorImpl implements EventExtractor {

	List<String> eventWords;
	List<String> argumentWords;
	List<String> argumentValueWords;

	@PostConstruct
	public void init() {
		if (this.eventWords == null) {
			this.eventWords = this
					.loadWords("e:/ming616/data/event/event_words.txt");
			this.argumentWords = this
					.loadWords("e:/ming616/data/event/argument_words.txt");
			this.argumentValueWords = this
					.loadWords("e:/ming616/data/event/argument_value_words.txt");
		}
	}

	private List<String> loadWords(String fileName) {
		List<String> words = null;
		try {
			words = FileUtils.readLines(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return words;
	}

	public List<NLPWord> extract(List<NLPWord> sentence) {
		for (NLPWord nlpWord : sentence) {
			if (this.eventWords.contains(nlpWord.getText())) {
				nlpWord.setEventLabel("B-Event");
			} else if (this.argumentWords.contains(nlpWord.getText())) {
				nlpWord.setEventLabel("B-Attribute");
			} else if (this.argumentValueWords.contains(nlpWord.getText())) {
				nlpWord.setEventLabel("B-AttributeValue");
			}
		}
		return sentence;
	}

}
