package net.ming616.nlp.extraction.service;

import java.util.List;

import net.ming616.nlp.qa.model.Question;

/**
 * question extractor for http://wenwen.soso.com/
 * 
 * @author liuxiaoming
 * 
 */
public interface WenwenExtractor {

	List<String> getQuestionURLList(String baseURL);

	Question getQuestion(String productURL);

}
