package net.ming616.nlp.extraction.service;

import java.util.Map;

import net.ming616.nlp.extraction.model.JingdongProduct;

/**
 * question extractor for http://zhidao.baidu.com
 * 
 * @author liuxiaoming
 * 
 */
public interface ZhidaoExtractor {

	Map<String, String> getQuestionList(String baseURL);

	JingdongProduct getQuestion(String productURL);

}
