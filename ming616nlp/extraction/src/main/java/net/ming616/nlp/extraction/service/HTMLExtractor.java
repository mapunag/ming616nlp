package net.ming616.nlp.extraction.service;

import java.util.Map;

public interface HTMLExtractor {

	Map<String, String> getContent(String url, Map<String, String> selectors);

}
