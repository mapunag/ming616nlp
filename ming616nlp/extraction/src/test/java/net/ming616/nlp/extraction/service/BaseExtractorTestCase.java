package net.ming616.nlp.extraction.service;

import org.apache.log4j.Logger;

import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.hit.service.HITManager;
import net.ming616.nlp.ictclas.service.ICTCLASManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-ictclas.xml",
		"classpath:applicationContext-hit.xml" })
public class BaseExtractorTestCase extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BaseExtractorTestCase.class);

	@Autowired
	ICTCLASManager ictclasManager;

	@Autowired
	HITManager hitManager;

	String text;

	List<NLPWord> sentence;

	void preProcess() {
		List<NLPWord> result = this.ictclasManager.segmentToNLPWord(this.text);
		this.sentence = this.hitManager.parse(result);
		if (logger.isInfoEnabled()) {
			logger.info("sentence=" + sentence); //$NON-NLS-1$
		}
	}

}
