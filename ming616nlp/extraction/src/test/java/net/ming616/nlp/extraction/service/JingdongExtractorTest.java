package net.ming616.nlp.extraction.service;

import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.extraction.model.JingdongProduct;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-extraction.xml" })
public class JingdongExtractorTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JingdongExtractorTest.class);

	@Autowired
	JingdongExtractor jingdongExtractor;

	@Test
	public void testGetProductURLList() {
		String url = "http://www.360buy.com/products/670-677-000.html";
		Map<String, String> itemMap = this.jingdongExtractor
				.getProductURLList(url);
		for (Entry<String, String> item : itemMap.entrySet()) {
			JingdongProduct product = this.jingdongExtractor.getProduct(item
					.getValue());
			if (logger.isInfoEnabled()) {
				logger.info("product=" + product); //$NON-NLS-1$
			}
		}

	}

}
