package net.ming616.nlp.extraction.service;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-extraction.xml" })
public class EventExtractorTest extends BaseExtractorTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EventExtractorTest.class);

	@Autowired
	EventExtractor eventExtractor;

	@Before
	public void before() {
		this.text = "19Ӣ�������ʾ��������Ǯ��";
		this.preProcess();
	}

	@Test
	public void testEventExtract() {
		List<NLPWord> events = this.eventExtractor.extract(this.sentence);
		if (logger.isInfoEnabled()) {
			logger.info("events=" + events); //$NON-NLS-1$
		}
	}
}
