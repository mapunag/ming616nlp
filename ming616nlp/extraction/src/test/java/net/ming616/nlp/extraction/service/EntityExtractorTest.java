package net.ming616.nlp.extraction.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.common.util.NLPUtils;
import net.ming616.nlp.qa.model.Question;
import net.ming616.nlp.qa.service.QuestionManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = {
		"classpath:applicationContext-extraction.xml",
		"classpath:applicationContext-qa.xml" })
public class EntityExtractorTest extends BaseExtractorTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntityExtractorTest.class);

	@Autowired
	EntityExtractor entityExtractor;

	@Autowired
	EventExtractor eventExtractor;

	@Autowired
	QuestionManager questionManager;

	String questionFile = "E:/ming616/data/comparative/comparative_question.txt";

	String outFile = "E:/ming616/data/comparative/extractResult.txt";

	List<String> lines = null;

	OutputStream output;

	@Before
	public void before() {
		try {
			lines = FileUtils.readLines(new File(this.questionFile),"UTF-8");
			output = new FileOutputStream(this.outFile, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testExtract() {
		int i =1;
		for (String line : lines) {
			if (logger.isInfoEnabled()) {
				logger.info("line"+(i++)+"=" + line); //$NON-NLS-1$
			}
			if (StringUtils.isNotBlank(line)) {
				List<NLPWord> words = this.ictclasManager
						.segmentToNLPWord(line);
				words = this.hitManager.parse(words);
				words = this.entityExtractor.extract(words);
				words = this.eventExtractor.extract(words);
				String questionString = NLPUtils.toString(words);
				try {
					IOUtils.write(questionString, output);
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
		}
	}

	@Test
	@Ignore
	public void testExtractAllQuestion() {
		List<Question> questions = this.questionManager.getAllDistinct();
		for (Question q : questions) {
			String qText = q.getTitle();
			if (StringUtils.isNotBlank(qText)) {
				List<NLPWord> words = this.ictclasManager
						.segmentToNLPWord(qText);
				words = this.hitManager.parse(words);
				words = this.entityExtractor.extract(words);
				//words = this.eventExtractor.extract(words);
				String questionString = NLPUtils.toString(words);
				if (logger.isInfoEnabled()) {
					logger.info(q.getId() + "=" + questionString); //$NON-NLS-1$
				}
				try {
					IOUtils.write(questionString, output);
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
		}
	}
}
