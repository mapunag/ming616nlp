package net.ming616.nlp.extraction.service;

import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.qa.model.Question;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-extraction.xml" })
public class WenwenExtractorTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(WenwenExtractorTest.class);

	@Autowired
	WenwenExtractor wenwenExtractor;

//	@Test
	public void testGetQuestion() {
		String url = "http://wenwen.soso.com/z/q299068210.htm";
		Question q = this.wenwenExtractor.getQuestion(url);
		if (logger.isInfoEnabled()) {
			logger.info("testGetQuestion() - Question q=" + q); //$NON-NLS-1$
		}
	}

	@Test
	public void testGetQuestionURLList() {
		List<String> list = this.wenwenExtractor
				.getQuestionURLList("http://wenwen.soso.com/z/ShowTagAutoAnswer.e?sp=319094784&sp=S%E6%98%BE%E7%A4%BA%E5%99%A8&sp=S%E6%98%BE%E7%A4%BA%E5%99%A8&ch=fl.tag&ch=fl.tag");
		for (String url : list) {
			Question q = this.wenwenExtractor.getQuestion(url);
			if (logger.isInfoEnabled()) {
				logger.info("Question q=" + q); //$NON-NLS-1$
			}
		}
	}
}
