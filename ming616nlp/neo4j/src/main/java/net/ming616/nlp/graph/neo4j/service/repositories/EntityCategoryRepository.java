package net.ming616.nlp.graph.neo4j.service.repositories;

import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.EntityCategory;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;

public interface EntityCategoryRepository extends
		GraphRepository<EntityCategory>, NamedIndexRepository<EntityCategory>,
		RelationshipOperationsRepository<EntityCategory> {

	EntityCategory findById(String id);

	Set<EntityCategory> findByNameLike(String name);
}
