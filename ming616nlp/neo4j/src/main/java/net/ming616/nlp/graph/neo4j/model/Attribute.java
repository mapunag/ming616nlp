package net.ming616.nlp.graph.neo4j.model;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class Attribute extends BaseNode {

	// @RelatedTo(type = "IS", elementClass = AttributeValue.class)
	// AttributeValue attributeValue;

	@RelatedTo(type = "BeongTo", elementClass = Entity.class)
	Entity entity;

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "AttributeNameIndex")
	String attributeName;

	@GraphId
	Long nodeId;

	@Indexed(indexName = "type")
	String type = String.valueOf(NodeType.Attribute);;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attribute other = (Attribute) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Entity getEntity() {
		return this.entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

}
