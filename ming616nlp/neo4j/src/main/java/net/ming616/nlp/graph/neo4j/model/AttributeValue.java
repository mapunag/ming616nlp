package net.ming616.nlp.graph.neo4j.model;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class AttributeValue extends BaseNode {
	@RelatedTo(type = "IS", elementClass = Attribute.class)
	Attribute attribute;

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "AttributeValueIndex")
	String name;

	@GraphId
	Long nodeId;

	@Indexed(indexName = "type")
	String type = String.valueOf(NodeType.AttributeValue);

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributeValue other = (AttributeValue) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}

	public Attribute getAttribute() {
		return attribute;
	};

	public String getName() {
		return name;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public void setType(String type) {
		this.type = type;
	}

}
