package net.ming616.nlp.graph.neo4j.service.repositories;

import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.Attribute;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;

public interface AttributeRepository extends GraphRepository<Attribute>,
		NamedIndexRepository<Attribute>,
		RelationshipOperationsRepository<Attribute> {
	Attribute findById(String id);

	Set<Attribute> findByAttributeNameLike(String name);
}
