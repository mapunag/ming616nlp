package net.ming616.nlp.extraction.service;

import java.util.List;

import net.ming616.nlp.extraction.model.JingdongProduct;

/**
 * product extractor for http://www.360buy.com
 * 
 * @author liuxiaoming
 * 
 */
public interface JingdongExtractor {

	List<String> getProductURLList(String baseURL);

	JingdongProduct getProduct(String productURL);

}
