package net.ming616.nlp.graph.neo4j.model;

public enum NodeType {
	EntityCategory, Entity, Attribute, AttributeValue

}
