package net.ming616.nlp.graph.neo4j.model;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class EntityCategory extends BaseNode {

	@RelatedTo(elementClass = EntityCategory.class, type = "Belong_To", direction = Direction.INCOMING)
	Set<EntityCategory> children = new HashSet<EntityCategory>();

	@RelatedTo(elementClass = EntityCategory.class, type = "Belong_To", direction = Direction.INCOMING)
	EntityCategory parent;

	public EntityCategory getParent() {
		return parent;
	}

	public void setParent(EntityCategory parent) {
		this.parent = parent;
	}

	@RelatedTo(elementClass = Entity.class, type = "Belong_To", direction = Direction.INCOMING)
	Set<Entity> entities = new HashSet<Entity>();

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "EntityCategoryIndex")
	String name;

	@GraphId
	Long nodeId;

	@Indexed(indexName = "type")
	final String type = String.valueOf(NodeType.EntityCategory);

	String url;

	public void addChild(EntityCategory c) {
		this.children.add(c);
	}

	public void addEntity(Entity e) {
		this.entities.add(e);
	}

	public Set<EntityCategory> getChildren() {
		return children;
	}

	public Set<Entity> getEntities() {
		return entities;
	}

	public String getName() {
		return name;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

	public void setChildren(Set<EntityCategory> children) {
		this.children = children;
	}

	public void setEntities(Set<Entity> entities) {
		this.entities = entities;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
