package net.ming616.nlp.graph.neo4j.model;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class Entity extends BaseNode {

	// @RelatedTo(type = "HAS", elementClass = Attribute.class)
	// Set<Attribute> attributes;

	@RelatedTo(type = "BelongTo", elementClass = EntityCategory.class)
	EntityCategory entityCategory;

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "EntityIndex")
	String entityName;

	@GraphId
	Long nodeId;

	@Indexed(indexName = "type")
	String type = String.valueOf(NodeType.Entity);

	String url;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}

	// public Set<Attribute> getAttributes() {
	// if (null == this.attributes) {
	// this.attributes = new HashSet<Attribute>();
	// }
	// return attributes;
	// }

	public EntityCategory getEntityCategory() {
		return entityCategory;
	}


	public Long getNodeId() {
		return nodeId;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}

	// public void setAttributes(Set<Attribute> attributes) {
	// this.attributes = attributes;
	// }

	public void setEntityCategory(EntityCategory entityCategory) {
		this.entityCategory = entityCategory;
	}


	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
