package net.ming616.nlp.graph.neo4j.service;

public class Chunk {

	String type;
	String text;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
