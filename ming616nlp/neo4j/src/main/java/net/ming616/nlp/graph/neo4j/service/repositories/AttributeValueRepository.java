package net.ming616.nlp.graph.neo4j.service.repositories;

import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.AttributeValue;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;

public interface AttributeValueRepository extends
		GraphRepository<AttributeValue>, NamedIndexRepository<AttributeValue>,
		RelationshipOperationsRepository<AttributeValue> {
	AttributeValue findById(String id);

	Set<AttributeValue> findByNameLike(String name);
}
