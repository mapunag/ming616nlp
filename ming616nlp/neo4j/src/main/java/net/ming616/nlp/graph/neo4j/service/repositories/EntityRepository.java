package net.ming616.nlp.graph.neo4j.service.repositories;

import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.Entity;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;

public interface EntityRepository extends GraphRepository<Entity>,
		NamedIndexRepository<Entity>, RelationshipOperationsRepository<Entity> {

	Entity findById(String id);

	Set<Entity> findByEntityNameLike(String name);
}
