package net.ming616.nlp.extraction.model;

import org.neo4j.graphdb.RelationshipType;

public enum ProductRelation implements RelationshipType {
	Include, BelongTo, HasAttribute, HasValue
}
