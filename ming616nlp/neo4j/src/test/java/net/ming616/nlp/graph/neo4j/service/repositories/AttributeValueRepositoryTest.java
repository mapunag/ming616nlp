package net.ming616.nlp.graph.neo4j.service.repositories;

import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.Attribute;
import net.ming616.nlp.graph.neo4j.model.AttributeValue;
import net.ming616.nlp.graph.neo4j.model.Entity;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext-neo4j.xml" })
@Transactional
public class AttributeValueRepositoryTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AttributeValueRepositoryTest.class);

	@Autowired
	AttributeValueRepository attributeValueRepository;

	@Autowired
	AttributeRepository attributeRepository;

	@Autowired
	EntityRepository entityRepository;

	AttributeValue result;

	@Before
	public void setUp() throws Exception {

		Entity e = new Entity();
		e.setEntityName("属性值查询测试");
		e = this.entityRepository.save(e);

		Attribute a = new Attribute();
		a.setAttributeName("属性值查询测试");
		a.setEntity(e);
		this.attributeRepository.save(a);

		AttributeValue v = new AttributeValue();
		v.setName("属性值查询测试");
		v.setAttribute(a);

		this.result = this.attributeValueRepository.save(v);
	}

	@After
	public void tearDown() throws Exception {
		this.attributeValueRepository.delete(this.result);
	}

	@Test
	public void testFindByNameLike() {
		Set<AttributeValue> found = this.attributeValueRepository
				.findByNameLike("*" + "查询" + "*");
		for (AttributeValue v : found) {
			logger.info(v);
		}
	}

}
