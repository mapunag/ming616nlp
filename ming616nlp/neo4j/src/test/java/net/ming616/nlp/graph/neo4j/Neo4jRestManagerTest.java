package net.ming616.nlp.graph.neo4j;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.extraction.model.JingdongProduct;
import net.ming616.nlp.extraction.model.ProductRelation;
import net.ming616.nlp.extraction.service.JingdongExtractor;
import net.ming616.nlp.graph.neo4j.service.Chunk;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.beans.factory.annotation.Autowired;

public class Neo4jRestManagerTest extends BaseNeo4jTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(Neo4jRestManagerTest.class);

	@Autowired
	JingdongExtractor jingdongExtractor;

	public void testGetProducts() {
		List<String> urls = this.jingdongExtractor
				.getProductURLList("http://www.360buy.com/products/652-653-655.html");
		for (String url : urls) {
			JingdongProduct product = this.jingdongExtractor.getProduct(url);

			Map<String, Object> props = new HashMap<String, Object>();
			props.put("name", product.getName());
			props.put("url", product.getUrl());
			props.put("Type", "Entity");
			Node entityNode = this.restAPI.createNode(props);

			Map<String, String> attributes = product.getAttributes();
			for (Entry<String, String> entry : attributes.entrySet()) {

				String attribute = entry.getKey();
				Map<String, Object> attributeProps = new HashMap<String, Object>();
				attributeProps.put("name", attribute);
				attributeProps.put("Type", "Attribute");
				Node attributeNode = this.restAPI.createNode(attributeProps);
				logger.info(attributeNode);

				this.restAPI.createRelationship(entityNode, attributeNode,
						ProductRelation.HasAttribute, null);

				String value = entry.getValue();
				Map<String, Object> valueProps = new HashMap<String, Object>();
				valueProps.put("name", value);
				valueProps.put("Type", "AttributeValue");
				Node valueNode = this.restAPI.createNode(valueProps);
				logger.info(valueNode);

				this.restAPI.createRelationship(attributeNode, valueNode,
						ProductRelation.HasValue, null);

			}
			logger.info(entityNode);
		}
	}

	public void testCategory() {
		String root = "http://www.360buy.com/computer.html";
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("name", "电脑办公");
		props.put("url", root);
		Node rootNode = this.restAPI.createNode(props);
		try {
			Document doc = Jsoup.parse(new URL(root), 6000);
			Elements items = doc.select("#sortlist .mc div");
			for (Element item : items) {
				Element second = item.select("h3 a").first();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", second.text());
				map.put("url", second.attr("href"));
				Node secondNode = this.restAPI.createNode(map);
				this.restAPI.createRelationship(rootNode, secondNode,
						ProductRelation.Include, null);
				this.restAPI.createRelationship(secondNode, rootNode,
						ProductRelation.BelongTo, null);
				if (logger.isInfoEnabled()) {
					logger.info("testCategory() - Element second=" + second); //$NON-NLS-1$
				}
				Elements thirds = item.select("ul li a");
				for (Element third : thirds) {
					Map<String, Object> newMap = new HashMap<String, Object>();
					newMap.put("name", third.text());
					newMap.put("url", third.attr("href"));
					Node thirdNode = this.restAPI.createNode(map);
					this.restAPI.createRelationship(secondNode, thirdNode,
							ProductRelation.Include, null);
					this.restAPI.createRelationship(thirdNode, secondNode,
							ProductRelation.BelongTo, null);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testFind() {
		List<Chunk> chunks = new ArrayList<Chunk>();

		Chunk a = new Chunk();
		a.setType("AttributeValue");
		a.setText("2012年");

		Chunk b = new Chunk();
		b.setType("AttributeValue");
		b.setText("I9308");

		Chunk c = new Chunk();
		c.setType("Attribute");
		c.setText("CPU频率");

		Chunk d = new Chunk();
		d.setType("AttributeValue");
		d.setText("四核");

		chunks.add(a);
		chunks.add(b);
		chunks.add(c);
		chunks.add(d);

		Collection<Node> result = this.findEntity(chunks);
		for (Node node : result) {

			logger.info(Utils.ToString(node));
		}

	}

	@SuppressWarnings("unchecked")
	private Collection<Node> findEntity(List<Chunk> chunks) {
		Collection<Node> result = null;
		List<Collection<Node>> temp = new ArrayList<Collection<Node>>();
		for (Chunk chunk : chunks) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("typeQuery", "Type:" + chunk.getType());
			params.put("nameQuery", ".*" + chunk.getText() + ".*");
			String queryString = null;
			if ("Attribute".equals(chunk.getType())) {
				queryString = "START n=node:node_auto_index({typeQuery})"
						+ " MATCH n<--x" + " WHERE n.name=~{nameQuery}"
						+ " RETURN DISTINCT x";
			} else if ("AttributeValue".equals(chunk.getType())) {
				queryString = "START n=node:node_auto_index({typeQuery})"
						+ " MATCH n<--x<--y" + " WHERE n.name=~{nameQuery}"
						+ " RETURN DISTINCT y";
			}
			final Collection<Node> nodes = IteratorUtil
					.asCollection(this.queryEngine.query(queryString, params)
							.to(Node.class));
			logger.info(nodes);
			temp.add(nodes);
			result = nodes;

		}

		for (Collection<Node> c : temp) {
			result = CollectionUtils.retainAll(result, c);
		}
		logger.info(result);
		return result;

	}
}
