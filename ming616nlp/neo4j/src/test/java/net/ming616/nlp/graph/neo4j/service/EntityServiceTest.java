package net.ming616.nlp.graph.neo4j.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.ming616.nlp.graph.neo4j.model.Attribute;
import net.ming616.nlp.graph.neo4j.model.AttributeValue;
import net.ming616.nlp.graph.neo4j.model.Entity;
import net.ming616.nlp.graph.neo4j.model.NodeType;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeValueRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.EntityRepository;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({ "/applicationContext-extraction.xml",
		"/applicationContext-neo4j.xml" })
public class EntityServiceTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger

	.getLogger(EntityServiceTest.class);

	@Autowired
	EntityService entityService;

	@Autowired
	AttributeValueRepository attributeValueRepository;

	@Autowired
	AttributeRepository attributeRepository;

	@Autowired
	EntityRepository entityRepository;

	@Before
	public void setUp() throws Exception {

		Entity e = new Entity();

		e.setEntityName("三星Galaxy Tab P6200");
		e.setUrl("http://www.360buy.com/product/549547.html");
		e = this.entityRepository.save(e);
		// 商品产地=中国大陆//
		Attribute a = new Attribute();
		a.setAttributeName("商品产地");
		a.setEntity(e);
		a = this.attributeRepository.save(a);

		AttributeValue v = new AttributeValue();
		v.setName("中国大陆");
		v.setAttribute(a);
		v = this.attributeValueRepository.save(v);
		// 型号=P6200
		a = new Attribute();
		a.setAttributeName("型号");
		a.setEntity(e);
		a = this.attributeRepository.save(a);

		v = new AttributeValue();
		v.setName("P6200");
		v.setAttribute(a);
		v = this.attributeValueRepository.save(v);
		// 品牌=三星
		a = new Attribute();
		a.setAttributeName("品牌");
		a.setEntity(e);
		a = this.attributeRepository.save(a);

		v = new AttributeValue();
		v.setName("三星");
		v.setAttribute(a);
		v = this.attributeValueRepository.save(v);
		//

	}

	@Test
	public void test() {
		List<Chunk> chunks = new ArrayList<Chunk>();

		Chunk a = new Chunk();
		a.setType(String.valueOf(NodeType.AttributeValue));
		a.setText("三星");
		//
		Chunk b = new Chunk();
		b.setType(String.valueOf(NodeType.AttributeValue));
		b.setText("P6200");
		//

		Chunk d = new Chunk();
		d.setType(String.valueOf(NodeType.Attribute));
		d.setText("产地");

		chunks.add(a);
		chunks.add(b);
		chunks.add(d);

		Set<Entity> results = this.entityService.findEntityByChunks(chunks);
		for (Entity entity : results) {
			logger.info(entity.getNodeId() + "=" + entity.getEntityName());
		}
	}
}
