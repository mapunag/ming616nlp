package net.ming616.nlp.graph.neo4j.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ming616.nlp.extraction.model.JingdongProduct;
import net.ming616.nlp.extraction.service.JingdongExtractor;
import net.ming616.nlp.graph.neo4j.model.Attribute;
import net.ming616.nlp.graph.neo4j.model.AttributeValue;
import net.ming616.nlp.graph.neo4j.model.Entity;
import net.ming616.nlp.graph.neo4j.model.EntityCategory;
import net.ming616.nlp.graph.neo4j.model.NodeType;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeValueRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.EntityCategoryRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.EntityRepository;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.helpers.collection.ClosableIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext-extraction.xml",
		"/applicationContext-neo4j.xml" })
public class DataImportTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger

	.getLogger(DataImportTest.class);

	@Autowired
	EntityCategoryRepository entityCategoryRepository;

	@Autowired
	EntityRepository entityRepository;

	@Autowired
	AttributeRepository attributeRepository;

	@Autowired
	AttributeValueRepository attributeValueRepository;

	@Autowired
	JingdongExtractor jingdongExtractor;

	@Test
	public void testImportProducts() {
		List<String> urls = this.jingdongExtractor
				.getProductURLList("http://www.360buy.com/products/652-653-655.html");
		for (String url : urls) {
			importProduct(url, null);
		}
	}

	private Entity importProduct(String url, EntityCategory entityCategory) {
		JingdongProduct product = this.jingdongExtractor.getProduct(url);
		Entity entity = new Entity();
		entity.setType(String.valueOf(NodeType.Entity));
		entity.setEntityName(product.getName());
		entity.setUrl(product.getUrl());
		entity.setEntityCategory(entityCategory);
		if (null != entityCategory) {
			entityCategory.addEntity(entity);
		}
		Map<String, String> attributes = product.getAttributes();
		for (Entry<String, String> attributeEntry : attributes.entrySet()) {
			AttributeValue value = new AttributeValue();
			value.setName(StringUtils.left(attributeEntry.getValue(), 255));
			value = this.attributeValueRepository.save(value);

			Attribute attribute = new Attribute();
			attribute.setAttributeName(attributeEntry.getKey());
			attribute = this.attributeRepository.save(attribute);
			value.setAttribute(attribute);
			this.attributeValueRepository.save(value);

			attribute.setEntity(entity);

		}
		entity = this.entityRepository.save(entity);
		if (logger.isInfoEnabled()) {
			logger.info("testImportProducts() - Entity entity=" + entity); //$NON-NLS-1$
		}
		return entity;
	}

	@Test
	@Ignore
	public void testImportCategory() throws MalformedURLException, IOException {
		String rootUrl = "http://www.360buy.com/computer.html";
		EntityCategory root = new EntityCategory();
		root.setName("电脑办公");
		root.setUrl(rootUrl);
		root = this.entityCategoryRepository.save(root);
		Document doc = Jsoup.parse(new URL(rootUrl), 6000);
		Elements items = doc.select("#sortlist .mc div");
		for (Element item : items) {
			Element second = item.select("h3 a").first();
			EntityCategory secondNode = new EntityCategory();
			secondNode.setName(second.text());
			secondNode.setUrl(second.attr("href"));
			secondNode = this.entityCategoryRepository.save(secondNode);
			root.addChild(secondNode);
			this.entityCategoryRepository.save(root);
			Elements thirds = item.select("ul li a");
			for (Element third : thirds) {
				EntityCategory thirdNode = new EntityCategory();
				thirdNode.setName(third.text());
				thirdNode.setUrl(third.attr("href"));
				thirdNode = this.entityCategoryRepository.save(thirdNode);
				secondNode.addChild(thirdNode);
				this.entityCategoryRepository.save(secondNode);
			}
		}
	}

	public void importAll() {
		try {
			this.testImportCategory();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ClosableIterable<EntityCategory> allCategories = this.entityCategoryRepository
				.findAll();
		for (EntityCategory entityCategory : allCategories) {
			if (CollectionUtils.isEmpty(entityCategory.getChildren())) {
				List<String> urls = this.jingdongExtractor
						.getProductURLList(entityCategory.getUrl());
				for (String url : urls) {
					this.importProduct(url, entityCategory);
				}
				entityCategory = this.entityCategoryRepository
						.save(entityCategory);
			}
			if (logger.isInfoEnabled()) {
				logger.info(entityCategory); //$NON-NLS-1$
			}
		}
	}
}
