package net.ming616.nlp.graph.neo4j;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.ictclas.service.ICTCLASManager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({ "/applicationContext-ictclas.xml",
		"/applicationContext-neo4j.xml" })
public class QuestionTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(QuestionTest.class);

	String question = "联想扬天S500的显卡类型是什么？";

	@Autowired
	ICTCLASManager ictclasManager;

	@Test
	public void testSegment() {
		List<NLPWord> result = this.ictclasManager
				.segmentToNLPWord(this.question);
		if (logger.isInfoEnabled()) {
			logger.info("testSegment() - List<NLPWord> result=" + result); //$NON-NLS-1$
		}

	}

}
