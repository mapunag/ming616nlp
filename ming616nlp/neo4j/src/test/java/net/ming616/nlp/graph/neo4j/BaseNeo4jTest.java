package net.ming616.nlp.graph.neo4j;

import java.net.URISyntaxException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath:applicationContext-extraction.xml" })
public class BaseNeo4jTest extends AbstractJUnit4SpringContextTests {
	/**
	 * Logger for this class
	 */
	protected GraphDatabaseService restGraphDb = null;
	private static final String HOSTNAME = "localhost";
	private static final int PORT = 7474;
	public static final String SERVER_ROOT = "http://" + HOSTNAME + ":" + PORT;
	protected static final String SERVER_ROOT_URI = SERVER_ROOT + "/db/data/";
	protected RestAPI restAPI;
	protected RestCypherQueryEngine queryEngine;

	@BeforeClass
	public static void startDb() throws Exception {

	}

	@Before
	public void setUp() throws URISyntaxException {
		this.restGraphDb = new RestGraphDatabase(SERVER_ROOT_URI);
		this.restAPI = ((RestGraphDatabase) this.restGraphDb).getRestAPI();
		queryEngine = new RestCypherQueryEngine(this.restAPI);
	}

	@After
	public void tearDown() throws Exception {
		restGraphDb.shutdown();
	}

	@AfterClass
	public static void shutdownDb() {

	}

}
