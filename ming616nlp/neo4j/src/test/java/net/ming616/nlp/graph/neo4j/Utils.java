package net.ming616.nlp.graph.neo4j;

import org.neo4j.graphdb.Node;

public class Utils {

	public static final String ToString(Node node) {
		StringBuffer buffer = new StringBuffer();
		Iterable<String> keys = node.getPropertyKeys();
		buffer.append("Id=" + node.getId() + "\n");
		for (String key : keys) {
			buffer.append(key + "=" + node.getProperty(key) + "\n");
		}
		return buffer.toString();
	}
}
