package net.ming616.nlp.graph.neo4j.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import net.ming616.nlp.extraction.service.JingdongExtractor;
import net.ming616.nlp.graph.neo4j.model.Attribute;
import net.ming616.nlp.graph.neo4j.model.AttributeValue;
import net.ming616.nlp.graph.neo4j.model.Entity;
import net.ming616.nlp.graph.neo4j.model.EntityCategory;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.AttributeValueRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.EntityCategoryRepository;
import net.ming616.nlp.graph.neo4j.service.repositories.EntityRepository;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.helpers.collection.ClosableIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({ "/applicationContext-extraction.xml",
		"/applicationContext-neo4j.xml" })
public class DataPrepareTest {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger

	.getLogger(DataPrepareTest.class);

	@Autowired
	EntityCategoryRepository entityCategoryRepository;

	@Autowired
	EntityRepository entityRepository;

	@Autowired
	AttributeRepository attributeRepository;

	@Autowired
	AttributeValueRepository attributeValueRepository;

	@Autowired
	JingdongExtractor jingdongExtractor;

	String dir = "d:/ming616/experiment/data/";

	@Test
	public void testGenerateChunks() throws IOException {
		this.generateAttributeChunks();
		this.generateAttributeValueChunks();
		this.generateEntityCategoryChunks();
		this.generateEntityChunks();
	}

	private void generateEntityCategoryChunks() throws FileNotFoundException,
			IOException {
		OutputStream out = new FileOutputStream(this.dir
				+ "chunk_entityCategory.txt", true);
		ClosableIterable<EntityCategory> all = this.entityCategoryRepository
				.findAll();
		Set<String> chunks = new HashSet<String>();
		for (EntityCategory e : all) {
			chunks.add(e.getName() + "\n");
		}
		IOUtils.writeLines(chunks, null, out);
	}

	private void generateEntityChunks() throws FileNotFoundException,
			IOException {
		OutputStream out = new FileOutputStream(this.dir + "chunk_entity.txt",
				true);
		ClosableIterable<Entity> all = this.entityRepository.findAll();
		Set<String> chunks = new HashSet<String>();
		for (Entity e : all) {
			chunks.add(e.getEntityName() + "\n");
		}
		IOUtils.writeLines(chunks, null, out);
	}

	private void generateAttributeChunks() throws FileNotFoundException,
			IOException {
		OutputStream out = new FileOutputStream(this.dir
				+ "chunk_attribute.txt", true);
		ClosableIterable<Attribute> all = this.attributeRepository.findAll();
		Set<String> chunks = new HashSet<String>();
		for (Attribute a : all) {
			chunks.add(a.getAttributeName() + "\n");
		}
		IOUtils.writeLines(chunks, null, out);
	}

	private void generateAttributeValueChunks() throws FileNotFoundException,
			IOException {
		OutputStream out = new FileOutputStream(this.dir
				+ "chunk_attributeValue.txt", true);
		ClosableIterable<AttributeValue> all = this.attributeValueRepository
				.findAll();
		Set<String> chunks = new HashSet<String>();
		for (AttributeValue e : all) {
			chunks.add(e.getName() + "\n");
		}
		IOUtils.writeLines(chunks, null, out);
	}
}
