package net.ming616.nlp.segment.service;

import junit.framework.Assert;
import net.ming616.nlp.ictclas.service.ICTCLASManager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext-ictclas.xml" })
public class ICTCLASManagerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ICTCLASManagerTest.class);

	@Autowired
	ICTCLASManager ictclasManager;

	String sentence = "这是一个测试";

	@Test
	public void testSegment() {
		String result = this.ictclasManager.segment(sentence);
		if (logger.isInfoEnabled()) {
			logger.info("testSegment() - String result=" + result); //$NON-NLS-1$
		}
		Assert.assertEquals(true, true);
	}
}
