package net.ming616.nlp.ictclas.service;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

public interface ICTCLASManager {

	String segment(String sentence);

	List<NLPWord> segmentToNLPWord(String sentence);

}
