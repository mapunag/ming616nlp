package net.ming616.nlp.hit.utils;

import java.util.ArrayList;
import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

public class HITQuestion {
	String cont;

	Integer paraId;

	Integer sentId;

	String tag;

	List<NLPWord> words;

	public String getCont() {
		return cont;
	}

	public Integer getParaId() {
		return paraId;
	}

	public Integer getSentId() {
		return sentId;
	}

	public String getTag() {
		return tag;
	}

	public List<NLPWord> getWords() {
		if (this.words == null) {
			this.words = new ArrayList<NLPWord>();
		}
		return words;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public void setParaId(Integer paraId) {
		this.paraId = paraId;
	}

	public void setSentId(Integer sentId) {
		this.sentId = sentId;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setWords(List<NLPWord> words) {
		this.words = words;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sentId == null) ? 0 : sentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HITQuestion other = (HITQuestion) obj;
		if (sentId == null) {
			if (other.sentId != null)
				return false;
		} else if (!sentId.equals(other.sentId))
			return false;
		return true;
	}

}
