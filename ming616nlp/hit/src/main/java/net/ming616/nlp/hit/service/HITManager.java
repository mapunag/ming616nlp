package net.ming616.nlp.hit.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import net.ming616.nlp.common.model.NLPWord;

import org.jdom.JDOMException;

import edu.hit.ir.ltpService.LTML;

public interface HITManager {

	LTML analyze(List<String> words) throws JDOMException, IOException;

	List<Map<String, String>> analyzeToMap(String question)
			throws JDOMException, IOException;

	List<NLPWord> parse(List<NLPWord> sentence);

	String analyzeToString(String question) throws JDOMException, IOException;

}
