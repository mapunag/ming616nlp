package net.ming616.nlp.hit.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext-hit.xml",
		"/applicationContextTest-resource.xml" })
public class HitManagerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(HitManagerTest.class);

	@Autowired
	HITManager hitManager;

	String sentence = "�����ҵ�һ������";

	@Test
	public void test() {
		String result = null;
		try {
			result = this.hitManager.analyzeToString(sentence);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		if (logger.isInfoEnabled()) {
			logger.info("test() - String result=" + result); //$NON-NLS-1$
		}
	}
}
