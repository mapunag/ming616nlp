package net.ming616.nlp.rite.service;

import java.util.List;

import net.ming616.nlp.rite.model.RiteModel;

public interface NTCIRManager {
	List<RiteModel> getAllRitePairs();
}
