package net.ming616.nlp.rite.service;

import net.ming616.nlp.rite.model.RiteModel;

public interface LDCManager {

	RiteModel getPair(String doc);

}
