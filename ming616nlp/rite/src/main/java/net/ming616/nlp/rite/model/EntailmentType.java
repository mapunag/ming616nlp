package net.ming616.nlp.rite.model;

public enum EntailmentType {
	FORWARD, REVERSE, BIDIRECTION, CONTRADICTION, INDEPENDENCE
}
