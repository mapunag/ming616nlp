package net.ming616.nlp.rite.service.impl;

import net.ming616.nlp.rite.model.RiteModel;
import net.ming616.nlp.rite.service.LDCManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

@Service("ldcManager")
public class LDCManagerImpl implements LDCManager {

	public RiteModel getPair(String document) {
		Document doc = Jsoup.parse(document);
		String id = doc.select("DOC").first().attr("id");
		String headline = doc.select("HEADLINE").first().text();
		Element p = doc.select("P").first();
		RiteModel model = new RiteModel();
		model.setId(id);
		model.setHypo(headline);

		if (null != p) {
			String firstPara = doc.select("P").first().text();
			model.setText(firstPara);

		}
		return model;
	}
}
