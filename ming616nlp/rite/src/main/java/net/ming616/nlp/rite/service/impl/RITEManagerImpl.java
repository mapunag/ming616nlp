package net.ming616.nlp.rite.service.impl;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.hit.service.HITManager;
import net.ming616.nlp.hit.utils.LTPUtils;
import net.ming616.nlp.ictclas.service.ICTCLASManager;
import net.ming616.nlp.rite.service.RITEManager;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("riteManager")
public class RITEManagerImpl implements RITEManager {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(RITEManagerImpl.class);

	@Autowired
	ICTCLASManager ictclasManager;

	@Autowired
	HITManager hitManager;

	public String getRite(String text, String hypo) {
		List<NLPWord> textList = this.ictclasManager.segmentToNLPWord(text);
		textList = this.hitManager.parse(textList);
		if (logger.isInfoEnabled()) {
			logger.info("textList=" + LTPUtils.toString(textList)); //$NON-NLS-1$
		}

		List<NLPWord> hypoList = this.ictclasManager.segmentToNLPWord(hypo);
		hypoList = this.hitManager.parse(hypoList);
		if (logger.isInfoEnabled()) {
			logger.info("hypoList=" + LTPUtils.toString(hypoList)); //$NON-NLS-1$
		}
		return null;
	}

}
