package net.ming616.nlp.rite.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class RiteModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3774030382117845660L;
	String hypo;
	String id;
	String label;
	String text;

	public String getHypo() {
		return hypo;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getText() {
		return text;
	}

	public void setHypo(String hypo) {
		this.hypo = hypo;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setText(String text) {
		this.text = text;
	}

}
