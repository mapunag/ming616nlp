package net.ming616.nlp.rite.service;

public interface RITEManager {

	String getRite(String text, String hypo);

}
