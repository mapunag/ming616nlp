package net.ming616.nlp.rite.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.rite.model.RiteModel;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-rite.xml",
		"classpath:applicationContext-ictclas.xml",
		"classpath:applicationContext-hit.xml" })
public class LDCManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LDCManagerTest.class);

	@Autowired
	LDCManager ldcManager;

	List<String> docs;

	String file = "E:/ming616/ldc/ntcir8_xinhua/data/all/xin_cmn_200201";

	@Before
	public void before() {
		this.docs = new ArrayList<String>();
		try {
			Document doc = Jsoup.parse(new File(this.file), "UTF-8");
			Elements ds = doc.select("DOC");
			for (Element element : ds) {
				this.docs.add(element.toString());
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	public void testGetPair() {
		for (String doc : this.docs) {
			RiteModel model = this.ldcManager.getPair(doc);
			if (logger.isInfoEnabled()) {
				logger.info("testGetPair() - RiteModel model=" + model); //$NON-NLS-1$
			}
		}
	}

}
