package net.ming616.nlp.rite.service;

import java.util.List;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.rite.model.RiteModel;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-rite.xml",
		"classpath:applicationContext-ictclas.xml",
		"classpath:applicationContext-hit.xml" })
public class NTCIRManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(NTCIRManagerTest.class);

	String text;
	String hypo;

	@Autowired
	RITEManager riteManager;

	@Autowired
	NTCIRManager ntcirManager;

	@Test
	public void testGetRite() {
		List<RiteModel> list = this.ntcirManager.getAllRitePairs();
		for (RiteModel rite : list) {
			String result = this.riteManager.getRite(rite.getText(),
					rite.getHypo());
			if (logger.isInfoEnabled()) {
				logger.info("result=" + result); //$NON-NLS-1$
			}
			break;
		}

	}

}
