package net.ming616.nlp.rite.service;

import net.ming616.nlp.base.service.BaseManagerTestCase;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:applicationContext-rite.xml",
		"classpath:applicationContext-ictclas.xml",
		"classpath:applicationContext-hit.xml" })
public class RITEManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(RITEManagerTest.class);

	String text;
	String hypo;

	@Autowired
	RITEManager riteManager;

	@Before
	public void before() {
		this.text = "XBox是微软公司的产品";
		this.hypo = "XBox不是索尼公司的产品";
	}

	@Test
	public void testGetRite() {
		String result = this.riteManager.getRite(this.text, this.hypo);
		if (logger.isInfoEnabled()) {
			logger.info("result=" + result); //$NON-NLS-1$
		}

	}

}
