package net.ming616.nlp.common.util;

import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

public class NLPUtils {

	public static String toString(List<NLPWord> list) {
		StringBuffer buffer = new StringBuffer();
		if (list != null && list.size() > 0) {
			buffer.append("\n");
			for (NLPWord nlpWord : list) {
				buffer.append(nlpWord.toString() + "\n");
			}
		}
		return buffer.toString();
	}

	public static String toConLL(NLPWord word) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(word.getText() + "\t");
		buffer.append(word.getPos() + "\t");
		buffer.append(word.getEntityLabel() + "\t");
		buffer.append("\n");
		return buffer.toString();
	}

	public static String toConLL(List<NLPWord> list) {
		StringBuffer buffer = new StringBuffer();
		if (list != null && list.size() > 0) {
			for (NLPWord nlpWord : list) {
				buffer.append(toConLL(nlpWord));
			}
		}
		return buffer.toString();
	}

}
