package net.ming616.nlp.common.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class NLPWord {
	List<NLPWord> children = new ArrayList<NLPWord>();

	String entityLabel = "";
	String eventLabel = "";
	String extension;
	Integer id;
	boolean isPredicate;
	String ne;
	Integer parent;
	NLPWord ParentNode;
	String pos;
	String relate;
	List<SRLNode> slrs;
	String text;
	boolean used;
	String wsd;
	String wsdxp;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NLPWord other = (NLPWord) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	public List<NLPWord> getChildren() {
		return children;
	}

	public String getEntityLabel() {
		return entityLabel;
	}

	public String getEventLabel() {
		return eventLabel;
	}

	public String getExtension() {
		return extension;
	}

	public Integer getId() {
		return id;
	}

	public String getNe() {
		return ne;
	}

	public Integer getParent() {
		return parent;
	}

	public NLPWord getParentNode() {
		return ParentNode;
	}

	public String getPos() {
		return pos;
	}

	public String getRelate() {
		return relate;
	}

	public List<SRLNode> getSrls() {
		if (null == this.slrs) {
			slrs = new ArrayList<SRLNode>();
		}
		return slrs;
	}

	public String getText() {
		return text;
	}

	public String getWsd() {
		return wsd;
	}

	public String getWsdxp() {
		return wsdxp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	public boolean isPredicate() {
		return isPredicate;
	}

	public boolean isUsed() {
		return used;
	}

	public void setChildren(List<NLPWord> children) {
		this.children = children;
	}

	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}

	public void setEventLabel(String eventLabel) {
		this.eventLabel = eventLabel;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNe(String ne) {
		this.ne = ne;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public void setParentNode(NLPWord parentNode) {
		ParentNode = parentNode;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public void setPredicate(boolean isPredicate) {
		this.isPredicate = isPredicate;
	}

	public void setRelate(String relate) {
		this.relate = relate;
	}

	public void setSrls(List<SRLNode> slrs) {
		this.slrs = slrs;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public void setWsd(String wsd) {
		this.wsd = wsd;
	}

	public void setWsdxp(String wsdxp) {
		this.wsdxp = wsdxp;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();

		if (null != this.id) {
			buffer.append(this.id + "\t");
		} else {
			buffer.append("-\t");
		}

		if (StringUtils.isNotBlank(this.text)) {
			buffer.append(this.text + "\t");
		} else {
			buffer.append("-\t");
		}

		if (StringUtils.isNotBlank(this.pos)) {
			buffer.append(this.pos + "\t");
		} else {
			buffer.append("-\t");
		}

		if (StringUtils.isNotBlank(this.ne)) {
			buffer.append(this.ne + "\t");
		} else {
			buffer.append("-\t");
		}

		if (null != this.relate) {
			buffer.append(this.relate + "\t");
		} else {
			buffer.append("-\t");
		}

		if (StringUtils.isNotBlank(this.wsd)) {
			buffer.append(this.wsd + "\t");
		} else {
			buffer.append("-\t");
		}

		if (null != this.parent) {
			buffer.append(this.parent + "\t");
		} else {
			buffer.append("-\t");
		}

		if (null != this.entityLabel) {
			buffer.append(this.entityLabel + "\t");
		} else {
			buffer.append("-\t");
		}

		if (null != this.eventLabel) {
			buffer.append(this.eventLabel + "\t");
		} else {
			buffer.append("-\t");
		}
		//
		return buffer.toString();
	}

}
