package net.ming616.nlp.common.model;

import net.ming616.nlp.base.model.BaseModel;

public class NLPWordModel extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2306308526877562360L;

	String pos;

	String text;

	public String getPos() {
		return pos;
	}

	public String getText() {
		return text;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public void setText(String text) {
		this.text = text;
	}

}
