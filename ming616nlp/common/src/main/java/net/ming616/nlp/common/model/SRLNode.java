package net.ming616.nlp.common.model;

public class SRLNode {
	Integer begin;
	Integer end;

	Integer id;

	String type;
	public Integer getBegin() {
		return begin;
	}
	public Integer getEnd() {
		return end;
	}

	public Integer getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setBegin(Integer begin) {
		this.begin = begin;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("(" + this.type + ",");
		buffer.append(this.begin + ",");
		buffer.append(this.end + ")");
		return buffer.toString();
	}

}
