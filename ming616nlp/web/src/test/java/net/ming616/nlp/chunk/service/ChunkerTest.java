package net.ming616.nlp.chunk.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

public class ChunkerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ChunkerTest.class);

	String crf_home = "D:/ming616/CRF++-0.57/";

	String testDir = "D:/ming616/experiment/chunk";

	@Test
	public void testCRF() {
		String[] cmd = { this.crf_home + "crf_test.exe", "-m",
				"e:/ming616/data/entity/entity_label_model",
				"e:/ming616/data/entity/entity_label_test.txt",
				">e:/ming616/data/entity/result.txt" };
		Runtime run = Runtime.getRuntime();
		try {
			Process process = run.exec(cmd);
			InputStream inputStream = process.getInputStream();
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer,"GBK");
			String theString = writer.toString();
			if (logger.isInfoEnabled()) {
				logger.info("testCRF() - String theString=" + theString); //$NON-NLS-1$
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
