package net.ming616.nlp.conversation.service;

import net.ming616.nlp.base.service.BaseManagerTestCase;
import net.ming616.nlp.conversation.model.Conversation;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = {
		"classpath:applicationContextTest-resource.xml",
		"classpath:applicationContext-web.xml",
		"classpath:applicationContext-ictclas.xml",
		"classpath:applicationContext-extraction.xml",
		"classpath:applicationContext-ontology.xml",
		"classpath:applicationContext-hit.xml" })
public class ConversationManagerTest extends BaseManagerTestCase {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ConversationManagerTest.class);

	@Autowired
	ConversationManager conversationManager;

	@Test
	public void test() {
		Conversation conversation = new Conversation();
		conversation.setQuestion("联想扬天S500的显卡类型是什么？");
		conversation = this.conversationManager.getAnswer(conversation);
		if (logger.isInfoEnabled()) {
			logger.info("conversation=" + conversation); //$NON-NLS-1$
		}

	}

}
