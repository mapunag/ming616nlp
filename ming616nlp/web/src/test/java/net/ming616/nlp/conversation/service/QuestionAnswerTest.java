package net.ming616.nlp.conversation.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.common.util.NLPUtils;
import net.ming616.nlp.crfpp.CRFPPManager;
import net.ming616.nlp.hit.service.HITManager;
import net.ming616.nlp.ictclas.service.ICTCLASManager;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({ "/applicationContext-ictclas.xml",
		"/applicationContext-hit.xml", "/applicationContext-neo4j.xml",
		"/applicationContextTest-resource.xml" })
public class QuestionAnswerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(QuestionAnswerTest.class);

	@Autowired
	ICTCLASManager ictclasManager;

	@Autowired
	HITManager hitManager;

	String questionFile = "D:/ming616/experiment/data/question.txt";

	String tempTestFile = "D:/ming616/experiment/data/temp_out.txt";

	String modelFile = "e:/ming616/data/entity/entity_model";

	String outFile = "e:/ming616/data/entity/entity_label_out.txt";

	OutputStream crfTempFile = null;

	@Before
	public void setUp() {

	}

	@Test
	public void test() throws IOException {
		InputStream in = new FileInputStream(this.questionFile);
		List<String> lines = IOUtils.readLines(in);
		for (String line : lines) {
			List<NLPWord> hits = understand(line);
		}
	}

	private List<NLPWord> understand(String line) {
		List<NLPWord> segs = this.ictclasManager.segmentToNLPWord(line);
		List<NLPWord> hits = this.hitManager.parse(segs);
		try {
			this.crfTempFile = new FileOutputStream(this.tempTestFile);
			IOUtils.write(NLPUtils.toConLL(hits), this.crfTempFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String result = CRFPPManager
				.test(modelFile, this.tempTestFile, outFile);

		return hits;
	}
}
