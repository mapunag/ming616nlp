package net.ming616.nlp.conversation.service;

import net.ming616.nlp.conversation.model.Conversation;

public interface ConversationManager {

	Conversation getAnswer(Conversation question);

}
