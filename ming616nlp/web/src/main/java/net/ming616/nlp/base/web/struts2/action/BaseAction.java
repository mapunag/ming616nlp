package net.ming616.nlp.base.web.struts2.action;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author liuxiaoming
 * 
 */
@SuppressWarnings("serial")
public abstract class BaseAction extends ActionSupport implements SessionAware {

	/**
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());

	private Map<String, Object> session;

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession() {
		return session;
	}

}
