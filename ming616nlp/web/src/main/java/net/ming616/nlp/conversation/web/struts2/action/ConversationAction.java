package net.ming616.nlp.conversation.web.struts2.action;

import net.ming616.nlp.base.web.struts2.action.BaseAction;
import net.ming616.nlp.conversation.model.Conversation;
import net.ming616.nlp.conversation.service.ConversationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opensymphony.xwork2.Action;

@Service("conversationAction")
public class ConversationAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -125001624600987505L;

	Conversation conversation;

	@Autowired
	ConversationManager conversationManager;

	public String answer() {
		if (this.conversation == null) {
			this.conversation = new Conversation();
		}
		this.conversationManager.getAnswer(this.conversation);
		return Action.SUCCESS;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

}
