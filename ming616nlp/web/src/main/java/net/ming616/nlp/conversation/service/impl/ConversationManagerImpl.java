package net.ming616.nlp.conversation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ming616.nlp.common.model.NLPWord;
import net.ming616.nlp.conversation.model.Conversation;
import net.ming616.nlp.conversation.service.ConversationManager;
import net.ming616.nlp.hit.service.HITManager;
import net.ming616.nlp.ictclas.service.ICTCLASManager;
import net.ming616.nlp.owl.service.OntologyQueryManager;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ConversationManagerImpl implements ConversationManager {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ConversationManagerImpl.class);

	@Autowired
	ICTCLASManager ictclasManager;

	@Autowired
	HITManager hitManager;

	// @Autowired
	// EntityExtractor entityExtractor;

	@Autowired
	OntologyQueryManager ontologyQueryManager;

	public Conversation getAnswer(Conversation conversation) {
		String answer = "";
		if (StringUtils.isBlank(conversation.getQuestion())) {
			conversation.setAnswer("I don't know !");
		} else {
			List<NLPWord> words = this.ictclasManager
					.segmentToNLPWord(conversation.getQuestion());
			words = this.labelEntity(words);
			words = this.hitManager.parse(words);
			// words = this.entityExtractor.extract(words);
			List<String> values = this.getValues(words);
			List<OWLNamedIndividual> individuals = this.ontologyQueryManager
					.getIndividualsByValues(values);

			String property = "显卡类型";

			for (OWLNamedIndividual individual : individuals) {
				String value = this.ontologyQueryManager.getValueByProperty(
						property, individual);
				if (StringUtils.isNotBlank(answer)) {
					answer = answer + "," + value;
				} else {
					answer = value;
				}
			}

			conversation.setAnswer(answer);
			if (logger.isInfoEnabled()) {
				logger.info("individuals=" + individuals); //$NON-NLS-1$
			}

			conversation.setWords(words);
		}
		return conversation;
	}

	private List<String> getValues(List<NLPWord> words) {
		List<String> result = new ArrayList<String>();
		for (NLPWord nlpWord : words) {
			if (StringUtils.equalsIgnoreCase(nlpWord.getEntityLabel(),
					"B-AttributeValue")) {
				result.add(nlpWord.getText());
			}
		}
		return result;
	}

	private List<NLPWord> labelEntity(List<NLPWord> words) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("联想", "B-AttributeValue");
		map.put("扬天", "B-AttributeValue");
		map.put("S500", "B-AttributeValue");
		map.put("显卡", "B-Attribute");
		map.put("类型", "I-Attribute");

		for (NLPWord nlpWord : words) {
			String word = nlpWord.getText();
			nlpWord.setEntityLabel(map.get(word));
		}
		return words;

	}

}
