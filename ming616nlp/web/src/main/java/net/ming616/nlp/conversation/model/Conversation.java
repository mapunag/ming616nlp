package net.ming616.nlp.conversation.model;

import java.io.Serializable;
import java.util.List;

import net.ming616.nlp.common.model.NLPWord;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Conversation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String question;

	String answer;

	List<NLPWord> words;
	
	public List<NLPWord> getWords() {
		return words;
	}

	public void setWords(List<NLPWord> words) {
		this.words = words;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
