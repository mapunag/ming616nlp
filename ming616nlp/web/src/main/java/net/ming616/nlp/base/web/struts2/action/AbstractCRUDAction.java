package net.ming616.nlp.base.web.struts2.action;

import org.springframework.beans.factory.annotation.Value;

@SuppressWarnings( { "serial" })
public abstract class AbstractCRUDAction extends BaseAction {

	@Value("${page.length}")
	protected Integer limit = 20;

	protected Integer start = 0;

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public abstract String save();

	public abstract String remove();

	public abstract String update();

	public abstract String load();

	public abstract String browse();

}
