package net.ming616.nlp.crfpp;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.util.StringUtils;

public class CRFPPManager {

	private static String crf_home = "D:/ming616/CRF++-0.57/";

	public static String test(String modelFile, String testFile, String outFile) {
		String[] cmd = { crf_home + "crf_test.exe", "-v0", "-m" + modelFile,
				testFile, ">" + outFile };
		String result = null;
		try {
			Process process = Runtime.getRuntime().exec(cmd);
			InputStream inputStream = process.getInputStream();
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer, "GBK");
			result = writer.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) {
		String testFile = "e:/ming616/data/entity/entity_label_test.txt";
		String modelFile = "e:/ming616/data/entity/entity_model";
		String outFile = "e:/ming616/data/entity/entity_label_out.txt";
		String result = CRFPPManager.test(modelFile, testFile, outFile);
		String[] lines = StringUtils.split(result, "\n");
		for (String line : lines) {
			System.out.print(line);
		}
	}
}
